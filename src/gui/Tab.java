package gui;

import helperClasses.Accounts;
import helperClasses.Accounts.Account;
import helperClasses.SearchItem;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultCaret;

import client.FutRunnable;

import net.miginfocom.swing.MigLayout;

public class Tab extends JFrame {

	private JTextArea messageTextArea;
	private JPanel optionPanel, messagePanel;
	private JTabbedPane plotTabPane;
	
	
	private JComboBox<Account> combo;

	private JButton start, stop;
	private JTextField status;
	
	
	//Fut para
	private Accounts accs;

	public static void main(String[] args) {
		//UI thread
		SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	new Tab();
				
		    }
		});
		
		
		
	}

	public Tab() {
		accs = new Accounts();
		
		initComponents();
		
		
	}
	
	private void runThing(){
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {

				
				
				Timer t = new Timer(200, new ActionListener() {

					int count = 0;

					@Override
					public void actionPerformed(ActionEvent e) {
						printRawMessage("testMessage" + count++);
					}
				});
				t.start();
			}
		});
	}
	
	private void startAction(){
		SearchItem benteke = new SearchItem("Benteke", 184111, 750, 900, 1200, 1610796847);
		Accounts accs = new Accounts();
		//Thread t2 = new Thread(new FutRunnable(accs.skanses, benteke,messageTextArea));
		//t2.start();
		
		//SwingUtilities.invokeLater(new FutRunnable(accs.skanses, benteke,messageTextArea));
	}
	

	private void initComponents() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel mainPanel = new JPanel(new GridLayout(1, 0));
		Box leftPanel = new Box(BoxLayout.Y_AXIS);
		leftPanel.add(getLeftTopPanel());
		leftPanel.add(getMessagePanel());
		mainPanel.add(leftPanel);
		mainPanel.add(getRightPanel());
		this.add(mainPanel);
		this.pack();
		this.setTitle("Ultimate team buyer");
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	protected JPanel getLeftTopPanelOld() {
		optionPanel = new JPanel();
		optionPanel.setBorder(BorderFactory.createTitledBorder(null, "Configuration", TitledBorder.LEFT, TitledBorder.TOP, new Font("null", Font.BOLD, 12),
				Color.BLUE));
		JLabel label = new JLabel("Choose");
		label.setHorizontalAlignment(JLabel.RIGHT);
		optionPanel.add(label);
		optionPanel.add(new JSeparator(JSeparator.VERTICAL));
		JComboBox<String> comboBox = new JComboBox<String>(new String[] { "option1", "option2", "option3" });

		optionPanel.add(comboBox);
		optionPanel.add(new JLabel("Type"));
		optionPanel.add(new JTextField("3"));
		return optionPanel;
	}
	
	
	protected JPanel getLeftTopPanel() {
		MigLayout mLayout = new MigLayout("", "[grow,fill][grow,fill][grow,fill]");
		JPanel panel = new JPanel(mLayout);

		
		combo = new JComboBox<Account>(accs.getAccountsArr());
		combo.setSelectedIndex(0);
		
		
		start = new JButton("Start");
		stop = new JButton("Stop");
		status = new JTextField("Status");

		start.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				start.setEnabled(false);
				stop.setEnabled(true);
				status.setText("Started!");
				//runThing();
				combo.setEnabled(false);
				
				System.out.println(combo.getSelectedItem());
				//startAction();
			}
		});
		
		stop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				start.setEnabled(true);
				stop.setEnabled(false);
				combo.setEnabled(true);
				status.setText("Stopped!");
			}
		});
		panel.add(combo,"span");
		panel.add(start);
		panel.add(stop, "wrap");
		panel.add(status, "span");

		return panel;
	}
	
	

	protected JTabbedPane getRightPanel() {
		plotTabPane = new JTabbedPane();
		plotTabPane.add("Watchlist", new JPanel());
		plotTabPane.add("Tradepile", new JPanel());
		return plotTabPane;
	}

	protected JPanel getMessagePanel() {
		messagePanel = new JPanel(new GridLayout());
		messagePanel.setBorder(BorderFactory.createTitledBorder(null, "Status Console", TitledBorder.LEFT, TitledBorder.TOP, new Font("null", Font.PLAIN, 10),
				Color.BLUE));
		final JScrollPane sp = new JScrollPane(getMessageTextArea());
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		messagePanel.add(sp);
		return messagePanel;
	}

	protected JTextArea getMessageTextArea() {
		messageTextArea = new JTextArea("", 10, 19);
		messageTextArea.setEditable(false);
		messageTextArea.setFont(new Font(null, Font.PLAIN, 20));
		messageTextArea.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		DefaultCaret caret = (DefaultCaret) messageTextArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		return messageTextArea;
	}

	public void printRawMessage(String rawMessage) {
		messageTextArea.append(rawMessage + "\n");
	}
}