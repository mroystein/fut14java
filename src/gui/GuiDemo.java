package gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import net.miginfocom.swing.MigLayout;
 
public class GuiDemo extends JPanel implements ActionListener {
    protected JTextField textField;
    protected JTextArea textArea;
    private final static String newline = "\n";
 
    public GuiDemo() {
    	MigLayout mLayout = new MigLayout( "", // Layout Constraints
    		       "[grow][grow]", // Column constraints
    		       "[grow][grow]"); // Row constraints
       JPanel panel = new JPanel(mLayout);
       
       
      

       panel.add(new JButton("Start"));
       panel.add(new JButton("Stop"), "wrap");
       JTextField tf = new JTextField("Status");
       
       tf.setEditable(false);
       panel.add(tf,"span");
      
       
 
        textField = new JTextField(20);
        textField.addActionListener(this);
 
        textArea = new JTextArea(5, 20);
        textArea.setEditable(false);
        
        
        JPanel logPanel = new JPanel();
        JScrollPane scrollPane = new JScrollPane(textArea);
        //scrollPane.set
       
        scrollPane.setBorder(BorderFactory.createTitledBorder("Log"));
      
        //scrollPane.setB
        panel.add(scrollPane,"span");
        
        add(panel);
       
    }
 
    public void actionPerformed(ActionEvent evt) {
        String text = textField.getText();
        textArea.append(text + newline);
        textField.selectAll();
 
        //Make sure the new text is visible, even if there
        //was a selection in the text area.
        textArea.setCaretPosition(textArea.getDocument().getLength());
    }
 
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("TextDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        //Add contents to the window.
        frame.add(new GuiDemo());
 
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
 
    public static void main(String[] args) {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}