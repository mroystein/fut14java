package gui;

public class TestMainStuff {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int k=0;
		while(k<20000){
			System.out.println(k+" => "+findNearestValidBid(k));
			
			k+=37;
			if(k>1000){
				k+=373;
			}
			if(k>10000){
				k+=1137;
			}
		}
		

	}
	
	public static int findNearestValidBid(int price) {

		if (price < 1000) {
			// round to nearest 50
			return ((price + 49) / 50) * 50;

		}
		if (price < 10000) {
			return ((price + 99) / 100) * 100;
		}

		return 0;
	}

}
