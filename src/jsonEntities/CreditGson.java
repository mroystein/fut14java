package jsonEntities;

import com.google.gson.Gson;

public class CreditGson {

	
	private int credits;
	
	public int getCredits() {
		return credits;
	}
	public void setCredits(int credits) {
		this.credits = credits;
	}
	
	
	public static void main(String[] args) {
		String json = "{\"credits\":10494,\"currencies\":[{\"name\":\"COINS\",\"funds\":10494,\"finalFunds\":10494},{\"name\":\"TOKEN\",\"funds\":0,\"finalFunds\":0},{\"name\":\"POINTS\",\"funds\":0,\"finalFunds\":0}],\"unopenedPacks\":{\"preOrderPacks\":0,\"recoveredPacks\":0}}";
		CreditGson credits = new Gson().fromJson(json, CreditGson.class);
		System.out.println(credits.getCredits());

	}

}
