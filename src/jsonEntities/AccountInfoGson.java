package jsonEntities;

import java.util.ArrayList;

import jsonEntities.AccountInfoGson.UserAccountInfo.PersonGson;

public class AccountInfoGson {

	//{"userAccountInfo":{"personas":[{"personaId":948529859,"personaName":"rema100090","userClubList":[{"year":"2014","lastAccessTime":1389610244,"platform":"ps3","clubAbbr":"Rem","clubName":"RemaNorge","teamId":321,"established":1387485827,"divisionOnline":1,"badgeId":6000547,"skuAccessList":{"FFA14PS3":1389610244}}],"returningUser":0}]}}

	private UserAccountInfo userAccountInfo;

	public class UserAccountInfo {

		private ArrayList<PersonGson> personas;

		public class PersonGson {
			private String personaName;
			private String personaId;
			private ArrayList<userClub> userClubList;

			public String getName() {
				return personaName;
			}
			public String getPersonaId() {
				return personaId;
			}
			public String getClubName() {
				return userClubList.get(0).getClubName();
			}
			
			@Override
			public String toString() {
				return String.format("Name : %s | Id: %s | ClubName: %s", getName(),getPersonaId(),getClubName());
			}
			
			
			
			private class userClub{
				private String clubName;
				
				public String getClubName(){
					return clubName;
				}
				
			}

		}

	}

	public PersonGson getPerson() {
		return userAccountInfo.personas.get(0);
	}

}
