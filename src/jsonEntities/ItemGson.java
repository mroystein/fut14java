package jsonEntities;

public class ItemGson {

	private long tradeId, expires, sellerId;
	private int buyNowPrice, currentBid, startingBid;
	private String tradeState, bidState, sellerName;
	private ItemData itemData;

	public ItemData getItemData() {
		return itemData;
	}
	
	@Override
	public String toString() {
		
		return String.format("TradeId:%s, tradeState:%s, bidState:%s, startingBid:%s, currentBid:%s, expires:%s, Item { ResId:%s, ItemId:%s }",  getTradeId(),getTradeState(),getBidState(),getStartingBid(),getCurrentBid(),getExpires(),itemData.getResourceId(),itemData.getId());
	}

	public long getTradeId() {
		return tradeId;
	}

	public void setTradeId(long tradeId) {
		this.tradeId = tradeId;
	}

	/**
	 * Time in seconds
	 * @return
	 */
	public long getExpires() {
		return expires;
	}

	public void setExpires(long expires) {
		this.expires = expires;
	}

	public long getSellerId() {
		return sellerId;
	}

	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}

	public int getBuyNowPrice() {
		return buyNowPrice;
	}

	public void setBuyNowPrice(int buyNowPrice) {
		this.buyNowPrice = buyNowPrice;
	}

	public int getCurrentBid() {
		return currentBid;
	}

	public void setCurrentBid(int currentBid) {
		this.currentBid = currentBid;
	}

	public int getStartingBid() {
		return startingBid;
	}

	public void setStartingBid(int startingBid) {
		this.startingBid = startingBid;
	}

	/**
	 * "expired", "active", "closed"
	 * @return 
	 */
	public String getTradeState() {
		return tradeState;
	}

	public void setTradeState(String tradeState) {
		this.tradeState = tradeState;
	}

	/**
	 * "none", "highest", "outbid"
	 * @return
	 */
	public String getBidState() {
		return bidState;
	}

	public void setBidState(String bidState) {
		this.bidState = bidState;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public void setItemData(ItemData itemData) {
		this.itemData = itemData;
	}



	public class ItemData {

		private long id, timestamp, resourceId, assetId,contract;

		private String itemType, itemState;
		private int rating, teamid, lastSalePrice, rareflag;

		public long getAssetId() {
			return assetId;
		}

		public long getContract() {
			return contract;
		}
		public void setContract(long contract) {
			this.contract = contract;
		}
		
		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public long getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(long timestamp) {
			this.timestamp = timestamp;
		}

		public String getItemType() {
			return itemType;
		}

		public void setItemType(String itemType) {
			this.itemType = itemType;
		}

		public int getRating() {
			return rating;
		}

		public void setRating(int rating) {
			this.rating = rating;
		}

		public int getTeamid() {
			return teamid;
		}

		public void setTeamid(int teamid) {
			this.teamid = teamid;
		}
		// "free"
		public String getItemState() {
			return itemState;
		}

		public void setItemState(String itemState) {
			this.itemState = itemState;
		}

		public long getResourceId() {
			return resourceId;
		}

		public void setResourceId(long resourceId) {
			this.resourceId = resourceId;
		}

		public int getLastSalePrice() {
			return lastSalePrice;
		}

		public void setLastSalePrice(int lastSalePrice) {
			this.lastSalePrice = lastSalePrice;
		}

		public int getRareflag() {
			return rareflag;
		}

		public void setRareflag(int rareflag) {
			this.rareflag = rareflag;
		}

		public void setAssetId(long assetId) {
			this.assetId = assetId;
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
