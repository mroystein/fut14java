package jsonEntities;

import java.util.ArrayList;

import com.google.gson.Gson;

public class AuctionInfoGson {

	private int credits;
	private ArrayList<ItemGson> auctionInfo;
	
	public ArrayList<ItemGson> getAuctionInfo() {
		return auctionInfo;
	}
	
	public void setAuctionInfo(ArrayList<ItemGson> auctionInfo) {
		this.auctionInfo = auctionInfo;
	}
	
	public int getCredits() {
		return credits;
	}
	public void setCredits(int credits) {
		this.credits = credits;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String s = "{\"credits\":18434,\"auctionInfo\":[{\"tradeId\":0,\"itemData\":{\"id\":106076974159,\"timestamp\":1382901680,\"itemType\":\"player\",\"formation\":\"f5212\",\"rating\":81,\"untradeable\":false,\"teamid\":18,\"attributeList\":[{\"value\":83,\"index\":0},{\"value\":83,\"index\":1},{\"value\":56,\"index\":2},{\"value\":78,\"index\":3},{\"value\":42,\"index\":4},{\"value\":68,\"index\":5}],\"statsList\":[{\"value\":31,\"index\":0},{\"value\":17,\"index\":1},{\"value\":0,\"index\":2},{\"value\":0,\"index\":3},{\"value\":0,\"index\":4}],\"lifetimeStats\":[{\"value\":31,\"index\":0},{\"value\":17,\"index\":1},{\"value\":0,\"index\":2},{\"value\":0,\"index\":3},{\"value\":0,\"index\":4}],\"injuryGames\":0,\"itemState\":\"free\",\"resourceId\":1610663278,\"leagueId\":0,\"preferredPosition\":\"ST\",\"assetId\":50542,\"training\":0,\"lastSalePrice\":850,\"owners\":3,\"morale\":50,\"discardValue\":648,\"injuryType\":\"foot\",\"suspension\":0,\"fitness\":99,\"assists\":7,\"cardsubtypeid\":3,\"contract\":11,\"rareflag\":1,\"playStyle\":250,\"lifetimeAssists\":7,\"loyaltyBonus\":1},\"buyNowPrice\":0,\"currentBid\":0,\"offers\":0,\"startingBid\":0,\"tradeState\":null,\"bidState\":null,\"watched\":false,\"expires\":0,\"sellerName\":null,\"sellerEstablished\":0,\"sellerId\":0}],\"bidTokens\":{},\"duplicateItemIdList\":null,\"currencies\":null,\"errorState\":null}";
		System.out.println("AuctionInfo Test");
		Gson gson = new Gson();
		AuctionInfoGson list = gson.fromJson(s, AuctionInfoGson.class);
		
		System.out.println(list.getAuctionInfo().size());
		System.out.println("Credits :"+list.credits);
		for (ItemGson i:list.getAuctionInfo()){
			System.out.println(i.getItemData().getResourceId());
		}
	}
	
	

}


