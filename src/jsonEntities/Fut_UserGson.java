package jsonEntities;

import java.util.ArrayList;

import com.google.gson.Gson;

public class Fut_UserGson {
	private int personaId;
	private int credits;
	private String personaName;
	private String clubName;
	private String clubAbbr;
	
	private ArrayList<Currency> currencies;
	
	private class Currency{
		String name;
		int funds;
		int finalFunds;
	}
	
	
	public int getPersonaId() {
		return personaId;
	}


	public void setPersonaId(int personaId) {
		this.personaId = personaId;
	}


	public int getCredits() {
		return credits;
	}


	public void setCredits(int credits) {
		this.credits = credits;
	}


	public String getPersonaName() {
		return personaName;
	}


	public void setPersonaName(String personaName) {
		this.personaName = personaName;
	}


	public String getClubName() {
		return clubName;
	}


	public void setClubName(String clubName) {
		this.clubName = clubName;
	}


	public String getClubAbbr() {
		return clubAbbr;
	}


	public void setClubAbbr(String clubAbbr) {
		this.clubAbbr = clubAbbr;
	}


	public static void main(String[] args) {
		System.out.println("Fut_UserGson Test");
		String json = "{\"personaId\":948529859,\"credits\":7685,\"squadList\":{\"squad\":[{\"id\":0,\"valid\":true,\"manager\":[],\"personaId\":null,\"players\":[],\"rating\":61,\"actives\":[],\"starRating\":61,\"squadName\":\"Ealing Rangers\",\"formation\":\"f442\",\"kicktakers\":[],\"changed\":null,\"chemistry\":45,\"captain\":null,\"newSquad\":null}],\"activeSquadId\":0},\"personaName\":\"rema100090\",\"clubAbbr\":\"Rem\",\"clubName\":\"RemaNorge\",\"bidTokens\":{\"count\":0,\"updateTime\":0},\"currencies\":[{\"name\":\"COINS\",\"funds\":7685,\"finalFunds\":7685},{\"name\":\"TOKEN\",\"funds\":0,\"finalFunds\":0},{\"name\":\"POINTS\",\"funds\":0,\"finalFunds\":0}],\"divisionOffline\":1,\"divisionOnline\":1,\"unopenedPacks\":{\"preOrderPacks\":0,\"recoveredPacks\":0},\"actives\":[{\"id\":107379029976,\"timestamp\":1387485827,\"itemType\":\"stadium\",\"training\":0,\"resourceId\":1616812774,\"rating\":64,\"teamid\":0,\"untradeable\":true,\"cardsubtypeid\":10,\"injuryType\":\"head\",\"injuryGames\":0,\"suspension\":0,\"morale\":0,\"fitness\":0,\"assists\":0,\"lastSalePrice\":0,\"owners\":1,\"leagueId\":0,\"discardValue\":0,\"formation\":\"f433\",\"preferredPosition\":\"GK\",\"assetId\":180,\"itemState\":\"activeStadium\",\"attributeList\":[],\"statsList\":[{\"value\":0,\"index\":0},{\"value\":0,\"index\":1},{\"value\":0,\"index\":2},{\"value\":0,\"index\":3},{\"value\":0,\"index\":4}],\"lifetimeStats\":[{\"value\":0,\"index\":0},{\"value\":0,\"index\":1},{\"value\":0,\"index\":2},{\"value\":0,\"index\":3},{\"value\":0,\"index\":4}],\"contract\":0,\"rareflag\":0,\"lifetimeAssists\":0},{\"id\":107379029982,\"timestamp\":1387485827,\"itemType\":\"kit\",\"training\":0,\"resourceId\":1616913350,\"rating\":64,\"teamid\":81,\"untradeable\":true,\"cardsubtypeid\":9,\"injuryType\":\"head\",\"injuryGames\":0,\"suspension\":0,\"morale\":0,\"fitness\":0,\"assists\":0,\"lastSalePrice\":0,\"owners\":1,\"leagueId\":0,\"discardValue\":0,\"formation\":\"f433\",\"preferredPosition\":\"GK\",\"assetId\":14,\"itemState\":\"activeHomeKit\",\"attributeList\":[],\"statsList\":[{\"value\":0,\"index\":0},{\"value\":0,\"index\":1},{\"value\":0,\"index\":2},{\"value\":0,\"index\":3},{\"value\":0,\"index\":4}],\"lifetimeStats\":[{\"value\":0,\"index\":0},{\"value\":0,\"index\":1},{\"value\":0,\"index\":2},{\"value\":0,\"index\":3},{\"value\":0,\"index\":4}],\"contract\":0,\"rareflag\":0,\"lifetimeAssists\":0},{\"id\":107379029983,\"timestamp\":1387485827,\"itemType\":\"ball\",\"training\":0,\"resourceId\":1618732840,\"rating\":74,\"teamid\":0,\"untradeable\":true,\"cardsubtypeid\":30,\"injuryType\":\"head\",\"injuryGames\":0,\"suspension\":0,\"morale\":0,\"fitness\":0,\"assists\":0,\"lastSalePrice\":0,\"owners\":1,\"leagueId\":0,\"discardValue\":0,\"formation\":\"f433\",\"preferredPosition\":\"GK\",\"assetId\":59,\"itemState\":\"activeBall\",\"attributeList\":[],\"statsList\":[{\"value\":0,\"index\":0},{\"value\":0,\"index\":1},{\"value\":0,\"index\":2},{\"value\":0,\"index\":3},{\"value\":0,\"index\":4}],\"lifetimeStats\":[{\"value\":0,\"index\":0},{\"value\":0,\"index\":1},{\"value\":0,\"index\":2},{\"value\":0,\"index\":3},{\"value\":0,\"index\":4}],\"contract\":0,\"rareflag\":0,\"lifetimeAssists\":0},{\"id\":107379029993,\"timestamp\":1387485827,\"itemType\":\"custom\",\"training\":0,\"resourceId\":1616613283,\"rating\":63,\"teamid\":321,\"untradeable\":true,\"cardsubtypeid\":11,\"injuryType\":\"head\",\"injuryGames\":0,\"suspension\":0,\"morale\":0,\"fitness\":0,\"assists\":0,\"lastSalePrice\":0,\"owners\":1,\"leagueId\":0,\"discardValue\":0,\"formation\":\"f433\",\"preferredPosition\":\"GK\",\"assetId\":321,\"itemState\":\"activeBadge\",\"attributeList\":[],\"statsList\":[{\"value\":0,\"index\":0},{\"value\":0,\"index\":1},{\"value\":0,\"index\":2},{\"value\":0,\"index\":3},{\"value\":0,\"index\":4}],\"lifetimeStats\":[{\"value\":0,\"index\":0},{\"value\":0,\"index\":1},{\"value\":0,\"index\":2},{\"value\":0,\"index\":3},{\"value\":0,\"index\":4}],\"contract\":0,\"rareflag\":0,\"lifetimeAssists\":0},{\"id\":107379029995,\"timestamp\":1387485827,\"itemType\":\"kit\",\"training\":0,\"resourceId\":1617013193,\"rating\":64,\"teamid\":112110,\"untradeable\":true,\"cardsubtypeid\":9,\"injuryType\":\"head\",\"injuryGames\":0,\"suspension\":0,\"morale\":0,\"fitness\":0,\"assists\":0,\"lastSalePrice\":0,\"owners\":1,\"leagueId\":0,\"discardValue\":0,\"formation\":\"f433\",\"preferredPosition\":\"GK\",\"assetId\":15,\"itemState\":\"activeAwayKit\",\"attributeList\":[],\"statsList\":[{\"value\":0,\"index\":0},{\"value\":0,\"index\":1},{\"value\":0,\"index\":2},{\"value\":0,\"index\":3},{\"value\":0,\"index\":4}],\"lifetimeStats\":[{\"value\":0,\"index\":0},{\"value\":0,\"index\":1},{\"value\":0,\"index\":2},{\"value\":0,\"index\":3},{\"value\":0,\"index\":4}],\"contract\":0,\"rareflag\":0,\"lifetimeAssists\":0}],\"seasonTicket\":false,\"established\":\"1387485827\",\"reliability\":{\"reliability\":100,\"startedMatches\":0,\"finishedMatches\":0,\"matchUnfinishedTime\":0},\"trophies\":0,\"loss\":0,\"draw\":0,\"won\":0,\"purchased\":false,\"accountCreatedPlatformName\":\"PS3\"}";
		Gson gson = new Gson();
		Fut_UserGson u = gson.fromJson(json, Fut_UserGson.class);
		System.out.println(u.getCredits());
	}

}



