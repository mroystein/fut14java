package db;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

public class SQLiteJDBC {

	private String sDriver = "org.sqlite.JDBC";
	private String sUrl = "jdbc:sqlite:test.db";
	private Connection c;
	protected Statement stmt;


	public int iTimeout = 30;
	
	
	
	

	public SQLiteJDBC() {
	
	}

	
	public Connection getConnection(){
		return c;
	}
	public void executeSql(String sql) {
		try {
			if (getStatement() == null) {
				setStatement();
			}
			stmt.execute(sql);
			stmt.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void checkStatment(){
		if(c==null){
			try {
				setConnection();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(stmt==null){
			try {
				setStatement();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	
	public void setStatement() throws Exception {
		if (c == null) {
			setConnection();
		}
		stmt = c.createStatement();
		stmt.setQueryTimeout(iTimeout); // set timeout to 30 sec.
	}

	public Statement getStatement() {
		return stmt;
	}

	public void setConnection() throws Exception {
		Class.forName(sDriver);
		c = DriverManager.getConnection(sUrl);
	}

	public void createTable() {

	}

	public void closeConnection() {
		try {
			c.close();
		} catch (Exception ignore) {
		}
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {

		SQLiteJDBC db = new SQLiteJDBC();
		// db.dropTableIfExists(db.tableName);

		ArrayList<StaticPlayerJson> players = getPlayers();
		if (players == null) {
			System.err.println("ERRORRRR");
			return;
		}
		for (StaticPlayerJson p : players) {
			/*try {
				db.insertStaticPlayer(p);
			} catch (Exception e) {
				System.out.println(p.toString());
				e.printStackTrace();
				return;
			}*/
		}
		db.closeConnection();

	}

	private static ArrayList<StaticPlayerJson> getPlayers() {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		// gson.fr
		ArrayList<StaticPlayerJson> list = null;
		try {
			JsonReader reader = new JsonReader(new FileReader(new File("players.json")));
			ReadPlayerJson rPlayer = gson.fromJson(reader, ReadPlayerJson.class);
			System.out.println(rPlayer.getPlayers().size());
			list = rPlayer.getPlayers();

			System.out.println(list.size());

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
		return list;
	}

}
