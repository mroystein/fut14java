package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbClass {

	private String sDriverName = "org.sqlite.JDBC";
	private String sUrl = "jdbc:sqlite:test.db";
	private Connection conn;
	private Statement stmt;

	private String tableName = "PLAYERS";
	public int iTimeout = 30;

	public DbClass() throws Exception {
		setConnection();
	}

	private void setConnection() throws Exception {
		try {
			Class.forName(sDriverName);
		} catch (Exception e) {
			System.err.println(e);
			throw e;
		}
		conn = DriverManager.getConnection(sUrl);
		try {
			stmt = conn.createStatement();
		} catch (Exception e) {
			try {
				conn.close();
			} catch (Exception ignore) {
			}
			conn = null;
		}
	}

	public void closeConnection() {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (Exception ignore) {
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (Exception ignore) {
			}
		}
	}
	
	public ResultSet executeQuery(String instruction) throws SQLException {
        return stmt.executeQuery(instruction);
    }
	
	public void execute(String instruction) throws SQLException {
        stmt.executeUpdate(instruction);
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
