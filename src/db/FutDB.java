package db;

import helperClasses.SearchItem;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;

import client.SearchVar;

public class FutDB extends SQLiteJDBC {

	/**
	 * @param args
	 * 
	 */

	// SearchItem("kyleWalker", 188377, 2900, 3300, 3700, 1610801113);

	private String INV_TABLE_NAME = "INVENTORY";

	/**
	 * The real ID..
	 */
	private String INV_COL_MaskedDefId = "MaskedDefId"; 
	private String INV_COL_BID_MAX = "BID_MAX";
	private String INV_COL_BUY_MAX = "BUY_MAX";
	private String INV_COL_SELL_MIN = "SELL_MIN";
	private String INV_COL_SELL_MAX = "SELL_MAX";
	private String INV_COL_ResId = "RES_ID";
	private String INV_COL_TimeStamp = "TimeStamp";

	private String tableName = "PLAYERS";
	private String StatP_Col_ID = "ID";
	private String StatP_Col_FNAME = "FNAME";
	private String StatP_Col_LNAME = "LNAME";
	private String StatP_Col_RATING = "RATING";
	private String StatP_Col_NATION = "NATION";

	public FutDB() {
		createTableIfNotExists();
	}

	public void createTableIfNotExists() {
		String sql = "CREATE TABLE if not exists PLAYERS " + "(ID INT PRIMARY KEY     NOT NULL," + " FNAME           TEXT    , "
				+ " LNAME           TEXT    , " + " RATING            INT     NOT NULL, " + " NATION            INT     ) ";

		executeSql(sql);
		
		
		//dropTableIfExists(INV_TABLE_NAME);

		sql = String.format("CREATE TABLE if not exists %s " +
				"(%s int PRIMARY KEY NOT NULL, %s int,%s int,%s int,%s int,%s int, %s int);", 
				INV_TABLE_NAME,
				INV_COL_MaskedDefId ,
				INV_COL_BID_MAX, 
				INV_COL_BUY_MAX, 
				INV_COL_SELL_MIN, 
				INV_COL_SELL_MAX,  
				INV_COL_ResId,
				INV_COL_TimeStamp);
		executeSql(sql);
	}

	public void dropTableIfExists(String tableName) {
		String sql = "drop table if exists " + tableName;
		executeSql(sql);

	}

	public void getStaticPlayers(String lastName) {
		String sql = String.format("SELECT * FROM %s WHERE LNAME='%s' ", tableName, lastName);
		// db.executeQuery(sql);

	}
	
	public StaticPlayerJson getStaticPlayer(long resId) {
		String sql = String.format("SELECT * FROM %s WHERE %s=%s ", tableName, StatP_Col_ID,resId);
		System.out.println(sql);
		checkStatment();
		StaticPlayerJson p = null;
		try {
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {

				String fname = rs.getString(StatP_Col_FNAME);
				String lname = rs.getString(StatP_Col_LNAME);
				int id = rs.getInt(StatP_Col_ID);
				int nation = rs.getInt(StatP_Col_NATION);
				int rating = rs.getInt(StatP_Col_RATING);
				p = new StaticPlayerJson(id, rating, nation, fname, lname);
				System.out.println(p);

			}
			rs.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return p;
	}

	public void insertStaticPlayer(StaticPlayerJson p) throws Exception {
		if (getConnection() == null)
			setConnection();
		String sql = "INSERT OR REPLACE INTO PLAYERS (ID,FNAME,LNAME,RATING,NATION) VALUES (?, ?, ?, ?, ?);";
		PreparedStatement stmt = getConnection().prepareStatement(sql);
		stmt.setInt(1, p.getId());
		stmt.setString(2, p.getFirstName());
		stmt.setString(3, p.getLastName());
		stmt.setInt(4, p.getRating());
		stmt.setInt(5, p.getNation());
		

		// System.out.println(sql);

		stmt.executeUpdate();

	}
	
	
	//------------------- Inverntoy ------------------------
	
	

	public ArrayList<SearchItem> getInventory() {
		checkStatment();
		String sql = String.format("SELECT * FROM %s ", INV_TABLE_NAME);
		ArrayList<SearchItem> items = new ArrayList<>();

		try {
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				long maskedDefId = rs.getLong(INV_COL_MaskedDefId);
				
				
				int maxBid = rs.getInt(INV_COL_BID_MAX);
				int maxBN = rs.getInt(INV_COL_BUY_MAX);
				int minSell = rs.getInt(INV_COL_SELL_MIN);
				int maxSell = rs.getInt(INV_COL_SELL_MAX);
				long resId = rs.getLong(INV_COL_ResId);
				long timeStamp = rs.getLong(INV_COL_TimeStamp);
				
				
				SearchItem item =new SearchItem("", maxBid, maxBN, minSell, maxSell, maskedDefId, resId);
				item.setTimeStamp(timeStamp);
				
				
				StaticPlayerJson sp = getStaticPlayer(maskedDefId);
				//System.out.println(sp);
				item.setName(sp.getFirstName()+" "+sp.getLastName());
				//System.out.println(item);
				items.add(item);
				
				
			}
			rs.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return items;

	}
	
	public SearchItem getPlayerFromInventory(long maskedDefIdAsset) {
		checkStatment();
		
		SearchItem item =null;
		String sql = String.format("SELECT * FROM "+INV_TABLE_NAME+" WHERE "+INV_COL_MaskedDefId+"="+maskedDefIdAsset);
		System.out.println(sql);
		

		try {
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				long maskedDefId = rs.getLong(INV_COL_MaskedDefId);
				
				
				int maxBid = rs.getInt(INV_COL_BID_MAX);
				int maxBN = rs.getInt(INV_COL_BUY_MAX);
				int minSell = rs.getInt(INV_COL_SELL_MIN);
				int maxSell = rs.getInt(INV_COL_SELL_MAX);
				long resId = rs.getLong(INV_COL_ResId);
				long timeStamp = rs.getLong(INV_COL_TimeStamp);
				
				
				item =new SearchItem("", maxBid, maxBN, minSell, maxSell, maskedDefId, resId);
				item.setTimeStamp(timeStamp);
		
				StaticPlayerJson sp = getStaticPlayer(maskedDefId);
				//System.out.println(sp);
				item.setName(sp.getFirstName()+" "+sp.getLastName());
				rs.close();
				return item;
				
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return item;

	}
	
	public void insertOrUpdate(SearchItem item){
		SearchItem it = getPlayerFromInventory(item.getMaskedDefId());
		if(it==null){
			insertInventoryPlayer(item);
		}else{
			updateInventoyPlayer(item);
		}
	}
	
	

	public void insertInventoryPlayer(SearchItem p) {
		try{
		if (getConnection() == null)
			setConnection();
		 
		 String sql = String.format("INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s) VALUES(?,?, ?, ?, ?, ?, ?);",
				 INV_TABLE_NAME,
				 INV_COL_MaskedDefId,
				 INV_COL_BID_MAX,
				 INV_COL_BUY_MAX,
				 INV_COL_SELL_MIN,
				 INV_COL_SELL_MAX,
				 INV_COL_ResId,
				 INV_COL_TimeStamp);
		System.out.println(sql);
		
		PreparedStatement stmt = getConnection().prepareStatement(sql);
		stmt.setLong(1, p.getMaskedDefId());
		stmt.setInt(2, p.getMaxBid());
		stmt.setInt(3, p.getMaxBuyNow());
		stmt.setInt(4, p.getSellStart());
		stmt.setInt(5, p.getSellBuyNow());
		stmt.setLong(6, p.getResourceId());
		stmt.setLong(7, p.getTimeStamp());
		
		stmt.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	public void updateInventoyPlayer(SearchItem p){
		
		 String sql = String.format("UPDATE %s SET %s=%s, %s=%s, %s=%s, %s=%s, %s=%s WHERE %s=%s;",
				 INV_TABLE_NAME,
				 INV_COL_BID_MAX,p.getMaxBid(),
				 INV_COL_BUY_MAX,p.getMaxBuyNow(),
				 INV_COL_SELL_MIN,p.getSellStart(),
				 INV_COL_SELL_MAX,p.getSellBuyNow(),
				 INV_COL_TimeStamp,p.getTimeStamp(),
				 INV_COL_MaskedDefId,p.getMaskedDefId());
		 System.out.println(sql);
		 executeSql(sql);
		
		 /*String sql2 = "UPDATE "+ INV_TABLE_NAME+" SET "+INV_COL_BID_MAX+"="",%s,%s,%s,%s,%s WHERE "+INV_COL_ResId+"="+p.getResourceId()+";",
				INV_COL_MaskedDefId,INV_COL_BID_MAX,INV_COL_BUY_MAX,INV_COL_SELL_MIN,INV_COL_SELL_MAX,INV_COL_ResId);
		*/
		
	}

	

	

	public static void main(String[] args) {
		FutDB futDb = new FutDB();
		
		System.out.println("------- Inventory ------");
		ArrayList<SearchItem> items = futDb.getInventory();
		for(SearchItem item: items){
			System.out.println(item);
		}
		
		ArrayList<SearchItem> allItems = getUpToDateCards();
		for(SearchItem it : allItems){
			futDb.insertOrUpdate(it);
		}
		
		
		
		System.out.println("------- Inventory ------");
		items = futDb.getInventory();
		for(SearchItem item: items){
			System.out.println(item);
		}
	
		
	
		System.out.println("Done");
		//futDb.
		// futDb.getStaticPlayer("Walcott");

		
	}
	
	
	public static ArrayList<SearchItem> getUpToDateCards() {
		ArrayList<SearchItem> list = new ArrayList<SearchItem>();
		// Keep selling them
		SearchItem contract = new SearchItem("GoldContract", SearchVar.Category_Contract, SearchVar.Type_Development, SearchVar.Level_Gold, null, 250, 0, 450,
				550, 1615613742L, 0);
		contract.setBuyMore(false);

		// BuyNow Cheapest is 850 (a few) 186115
		SearchItem gibbs = new SearchItem("Gibbs", 186115, 550, 800, 900, 1610798851);
		System.out.println(gibbs.getMaskedDefId());
		
		
		// cheapest BuyNow 900 186115
		SearchItem benteke = new SearchItem("Benteke", 184111, 650, 900, 1200, 1610796847);

		SearchItem clichy = new SearchItem("Clichy", 152554, 2100, 2600, 2900, 1610765290);
		// resId=1610765290
		// maskedDef 152554

		SearchItem vertongen = new SearchItem("Vertongen", 172871, 450, 750, 950, 1610785607);

		// maskedDef 172871
		// resId 1610785607

		// david luis
		// mask 179944
		// res id 1610792680
		SearchItem luiz = new SearchItem("DavidLuiz", 179944, 8000, 9300, 9900, 1610792680);

		// walker
		// mask 179944
		// res id 1610792680
		SearchItem walker = new SearchItem("kyleWalker", 188377, 2900, 3300, 3700, 1610801113);

		// ramires
		// mask 184943
		// resid 1610797679
		SearchItem ramires = new SearchItem("Ramires", 184943, 5000, 5600, 5900, 1610797679);

		list.add(contract);
		list.add(gibbs);
		list.add(benteke);
		list.add(clichy);
		list.add(vertongen);
		list.add(luiz);
		 list.add(walker);
		list.add(ramires);
		Collections.shuffle(list);
		return list;
	}


}
