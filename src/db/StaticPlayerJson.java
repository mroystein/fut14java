package db;

import com.google.gson.annotations.SerializedName;

public class StaticPlayerJson {

	/*
	 * "id": 2, "r": 69, "n": 27, "f": "Giovanni", "l": "Pasquale"
	 */
	private int id;
	@SerializedName("r")
	private int rating;
	@SerializedName("n")
	private int nation;
	@SerializedName("f")
	private String firstName;
	@SerializedName("l")
	private String lastName;
	
	
	@Override
	public String toString() {
		return "StaticPlayerJson [id=" + id + ", rating=" + rating + ", nation=" + nation + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}
	public StaticPlayerJson(int id, int rating, int nation, String firstName, String lastName) {
		super();
		this.id = id;
		this.rating = rating;
		this.nation = nation;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public int getNation() {
		return nation;
	}
	public void setNation(int nation) {
		this.nation = nation;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	private void sanitizeInput(){
		
	}
	

}