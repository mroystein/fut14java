package db;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;

public class ReadPlayerJson {
	
	@SerializedName("Players")
	private ArrayList<StaticPlayerJson> players;
	
	public ReadPlayerJson() {
		// TODO Auto-generated constructor stub
	}
	
	public ArrayList<StaticPlayerJson> getPlayers() {
		return players;
	}
	
	public void setPlayers(ArrayList<StaticPlayerJson> players) {
		this.players = players;
	}

	
	
	

	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Gson gson = new Gson();
		// gson.fr
		try {
			JsonReader reader = new JsonReader(new FileReader(new File("players.json")));
			ReadPlayerJson rPlayer = gson.fromJson(reader, ReadPlayerJson.class);
			System.out.println(rPlayer.getPlayers().size());
			ArrayList<StaticPlayerJson> list = rPlayer.getPlayers();
			long largestId = 0;
			for(StaticPlayerJson p : list){
				if(p.getId()>largestId){
					largestId=p.getId();
				}
			}
			System.out.println(largestId);

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}

	}

}
