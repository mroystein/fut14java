package client;

import helperClasses.Accounts;
import helperClasses.Accounts.Account;
import helperClasses.SearchItem;

import java.util.ArrayList;

import jsonEntities.ItemGson;

public class FutMain {

	private Core core;

	public static int sleep_30Sec = 30000;
	public static int sleep_OneMin = 60000;
	public static int sleep_TwoMin = 120000;
	
	private ArrayList<SearchItem> cards = new ArrayList<>();
	private Accounts accs ;

	private Account myAcc;
	private SearchItem myCard;

	public FutMain(Account myAcc,SearchItem card ) {
		this.myAcc=myAcc;
		this.myCard=card;
		
		cards.add(card);//Should give a list of cards to search for..
		
		System.out.println(card);
		System.out.println(card.getProfit());
		
		accs = new Accounts();
		

	}
	
	public void runBot(){
		SearchItem card = myCard;

		
		System.out.println("FutMain start");

		core = new Core(myAcc.getEmail(), myAcc.getPassword(), myAcc.getQuestion());
		core.login(false);
		mySleep(5000);

		boolean b = true;
		while (b) {
			searchAndBuy(card);
			mySleep(sleep_30Sec);
			core.keepAlive();
			mySleep(2000);
			reListAndMoveContracts();
			mySleep(sleep_OneMin);
			core.keepAlive();
			mySleep(sleep_OneMin);
		}

		//
		// core.
		core.keepAlive();
	}
	

	public void searchAndBuy(SearchItem card) {
		
		if(card.getMaxBid()>=core.getCredits()){
			System.out.println("Not enough money. No point in searching");
			return;
		}
		
		
		System.out.println("Searching for " + card.getName());
		mySleep(333);
		int tradePileSize = core.getTradePile().size();
		System.out.println("Tradepile size:" + tradePileSize);
		mySleep(333);
		int watchListSize = core.getWatchList().size();
		System.out.println("Watchlist size:" + watchListSize);
		int tadeWatchSize = tradePileSize + watchListSize;
		mySleep(333);

		int currPage = 0; // Current page, increse by 16
		boolean keepSearching = true;

		while (keepSearching) {
			int searchMaxBid = calculatePrevBid(card.getMaxBid());
			ArrayList<ItemGson> items = null;//core.searchAuctions(card.getCategory(), card.getType(), card.getLevel(),card.getPlayStyle(), currPage, searchMaxBid,0);
			for (ItemGson item : items) {

				if(item.getItemData().getResourceId()!=card.getResourceId()){
					System.err.println("ID's are wrong!!");
				}
				
				if (tadeWatchSize > 29) {
					System.out.println("Trade/Watchpile is full. Stopping searching");
					return;
				}
				if(accs.isMyAccount(item.getSellerName())){
					System.out.println("Dont bid, highest bidder is own account");
					return;
				}
				

				int bid = calculateNextBid(item);
				if(bid>core.getCredits()){
					System.out.println("Not enough money. Stopping seraching..");
					return;
				}
				
				
				if (bid <= card.getMaxBid()) {
					System.out.println("Bidding " + bid + " on " + item);
					boolean success =  core.bid(item.getTradeId(), item.getItemData().getId(), bid);
					System.out.println("Success :" + success);
					if (success)
						tadeWatchSize++;
					mySleep(3330);

				}
				// System.out.println(item);
			}

			currPage += 16;
			mySleep(3000);// 3 sec
			if (items.size() < 16) {
				keepSearching = false;
				System.out.println("Finished searching.. No more items");
			}
		}
	}
	
	
	public void reListAndMove() {
		mySleep(333);
		ArrayList<ItemGson> tradepile = core.getTradePile();
		mySleep(333);
		ArrayList<ItemGson> watchlist = core.getWatchList();
		System.out.println("watchlist size :" + watchlist.size());
		// ------------ WatchList --------------------
		for (ItemGson item : watchlist) {

			// System.out.println(item);

			mySleep(1000);

			long tradeId = item.getTradeId();
			String tradeState = item.getTradeState();
			String bidState = item.getBidState();
			long itemId = item.getItemData().getId();

			if (bidState.equals("outbid") || (bidState.equals("none") && tradeState.equals("expired"))) {
				core.watchlistDelete(tradeId);
				System.out.println("Removed item from watchlist");

			} else if (bidState.equals("highest") && tradeState.equals("closed")) {
				// Won it

				core.sendToTradepile(itemId, tradeId);
				System.out.println("Sendt item to tradepile");

			} else {
				// Do nothing. Currently a bid on it..
			}

		}

		// ------------ Tradepile --------------------
		System.out.println("Tradepile size :" + tradepile.size());
		for (ItemGson item : tradepile) {
			// System.out.println(item);
			long itemId = item.getItemData().getId();
			long tradeId = item.getTradeId();
			String tradeState = item.getTradeState();
			String bidState = item.getBidState();
			int currentBid = item.getCurrentBid();
			
			SearchItem card = getSearchItem(item.getItemData().getResourceId());
			if(card==null){
				System.err.println("Card isnt in stock, dunno how to sell");
				return;
			}
			

			mySleep(500);

			if (tradeState == null || tradeState.equals("None")) {
				// Item was just bought and won, need to list it on market
				String s = "\t Item '"+card.getName()+"' was just bought and won, need to list it on market";
				System.out.println(s);
				
				core.sell(itemId, card.getSellStart(), card.getSellBuyNow(), 3600);

			} else if (tradeState.equals("expired") && currentBid == 0) {
				// Item has expired, and wasnt sold. Need to relist
				String s = "\t Item '"+card.getName()+"' has expired, and wasnt sold. Need to relist";
				System.out.println(s);

				core.sell(itemId, card.getSellStart(), card.getSellBuyNow(), 3600);

			} else if (tradeState.equals("closed")) {
				// Item has sold
				String s = "\t Sold item '"+card.getName()+"' for " + item.getCurrentBid() + " coins.";
				System.out.println(s);
				core.tradePileDelete(tradeId);
			}

		}
	}

	
	
	
	private SearchItem getSearchItem(long resourseId){
		for(SearchItem c : cards){
			if(c.getResourceId()==resourseId)
				return c;
		}
		return null;
	}
	
	
	
	
	
	
	

	public void searchAndBuy() {
		System.out.println("Search and buy");
		mySleep(333);
		int tradePileSize = core.getTradePile().size();
		System.out.println("Tradepile size:" + tradePileSize);
		mySleep(333);
		int watchListSize = core.getWatchList().size();
		System.out.println("Watchlist size:" + watchListSize);
		int tadeWatchSize = tradePileSize + watchListSize;
		mySleep(333);

		int currPage = 0; // Current page, increse by 16
		boolean keepSearching = true;

		while (keepSearching) {
			ArrayList<ItemGson> items = core.searchAuctions_Contract(currPage, 300);
			for (ItemGson item : items) {

				if (tadeWatchSize > 29) {
					System.out.println("Trade/Watchpile is full. Stopping searching");
					return;
				}

				if (isRearGoldContract(item)) {
					int bid = calculateNextBid(item);
					if (bid <= 300) {
						System.out.println("Bidding " + bid + " on " + item);
						boolean success = core.bid(item.getTradeId(), item.getItemData().getId(), bid);
						System.out.println("Success :" + success);
						if (success)
							tadeWatchSize++;
						mySleep(3330);
					}

				}
				// System.out.println(item);
			}

			currPage += 16;
			mySleep(3000);// 3 sec
			if (items.size() < 16) {
				keepSearching = false;
				System.out.println("Finished searching.. No more items");
			}
		}
	}

	public int calculateNextBid(ItemGson item) {
		int startingBid = item.getStartingBid();
		int currentBid = item.getCurrentBid();

		if (currentBid == 0)
			return startingBid;

		if (currentBid < 1000)
			return currentBid + 50;

		if (currentBid < 10000)
			return currentBid + 100;

		if (currentBid < 50000)
			return currentBid + 250;

		if (currentBid < 100000)
			return currentBid + 500;

		return currentBid + 1000;

	}
	
	/**
	 * For searching.. Search on 3000, will need 3100 bids
	 * @param maxBid
	 * @return
	 */
	public int calculatePrevBid(int maxBid) {
		if(maxBid<300)
			return 250;//Min bid
		
		if (maxBid < 1000)
			return maxBid - 50;

		if (maxBid < 10000)
			return maxBid - 100;

		if (maxBid < 50000)
			return maxBid - 250;

		if (maxBid < 100000)
			return maxBid - 500;

		return maxBid - 1000;

	}

	public boolean isRearGoldContract(ItemGson item) {
		if (item.getItemData().getResourceId() == 1615613742L) {
			if (item.getItemData().getRareflag() == 1) {
				return true;
			}
		}
		return false;

	}

	public void mySleep(int millisec) {

		try {
			Thread.sleep(millisec);// 1 sec
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void reListAndMoveContracts() {
		mySleep(333);
		ArrayList<ItemGson> tradepile = core.getTradePile();
		mySleep(333);
		ArrayList<ItemGson> watchlist = core.getWatchList();
		System.out.println("watchlist size :" + watchlist.size());
		// ------------ WatchList --------------------
		for (ItemGson item : watchlist) {

			// System.out.println(item);

			mySleep(1000);

			long tradeId = item.getTradeId();
			String tradeState = item.getTradeState();
			String bidState = item.getBidState();
			long itemId = item.getItemData().getId();

			if (bidState.equals("outbid") || (bidState.equals("none") && tradeState.equals("expired"))) {
				core.watchlistDelete(tradeId);
				System.out.println("Removed item from watchlist");

			} else if (bidState.equals("highest") && tradeState.equals("closed")) {
				// Won it

				core.sendToTradepile(itemId, tradeId);
				System.out.println("Sendt item to tradepile");

			} else {
				// Do nothing. Currently a bid on it..
			}

		}
		mySleep(333);
		//reload tradepile
		tradepile = core.getTradePile();
		mySleep(333);

		// ------------ Tradepile --------------------
		System.out.println("Tradepile size :" + tradepile.size());
		for (ItemGson item : tradepile) {
			// System.out.println(item);
			long itemId = item.getItemData().getId();
			long tradeId = item.getTradeId();
			String tradeState = item.getTradeState();
			String bidState = item.getBidState();
			int currentBid = item.getCurrentBid();

			mySleep(500);

			if (tradeState == null || tradeState.equals("None")) {
				// Item was just bought and won, need to list it on market
				String s = "\t Item was just bought and won, need to list it on market";
				System.out.println(s);
				core.sell(itemId, 450, 550, 3600);

			} else if (tradeState.equals("expired") && currentBid == 0) {
				// Item has expired, and wasnt sold. Need to relist
				String s = "\t Item has expired, and wasnt sold. Need to relist";
				System.out.println(s);

				core.sell(itemId, 450, 550, 3600);

			} else if (tradeState.equals("closed")) {
				// Item has sold
				String s = "\t Sold item for " + item.getCurrentBid() + " coins.";
				System.out.println(s);
				core.tradePileDelete(tradeId);
			}

		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*SearchItem card = new SearchItem("Hunter", SearchVar.Category_playStyle, SearchVar.Type_Training, null, SearchVar.PlayStyle_Hunter, 3100, 3800, 4000,1615615847,0);
		Accounts accs = new Accounts();
		
		FutMain fut = new FutMain(accs.askoy,card);
		fut.runBot();*/
		

	}

}
