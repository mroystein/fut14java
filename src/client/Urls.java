package client;

/*
 * 
 GET https://utas.s2.fut.ea.com:443/ut/game/fifa14/user () {}
 GET https://utas.s2.fut.ea.com:443/ut/game/fifa14/clientdata/pileSize () {}
 GET https://utas.s2.fut.ea.com:443/ut/game/fifa14/watchlist () {}
 GET https://utas.s2.fut.ea.com:443/ut/game/fifa14/tradepile () {}

 * 
 */

public class Urls {
	public String main_site = "https://www.easports.com";
	public String futweb = "http://www.easports.com/iframe/fut/?locale=en_US&baseShowoffUrl=http%3A%2F%2Fwww.easports.com%2Fuk%2Ffifa%2Ffootball-club%2Fultimate-team%2Fshow-off&guest_app_uri=http%3A%2F%2Fwww.easports.com%2Fuk%2Ffifa%2Ffootball-club%2Fultimate-team";
	public String fut_config = "http://www.easports.com/iframe/fut/bundles/futweb/web/flash/xml/site_config.xml";
	public String fut_host = "https://utas.s2.fut.ea.com:443";
	public String fut_home = "http://www.easports.com/uk/fifa/football-club/ultimate-team";
	public String fut_question = "http://www.easports.com/iframe/fut/p/ut/game/fifa14/phishing/question?_=";// +time
	public String fut_validate = "http://www.easports.com/iframe/fut/p/ut/game/fifa14/phishing/validate";

	public FutUrls futUrls;
	public String shards = "http://www.easports.com/iframe/fut/p/ut/shards?_=";

	public String login = "";

	public Urls() {
		futUrls = new FutUrls();
	}

	public void setLoginUrl(String url) {
		login = url;
	}

	public void updateUrls() {

	}

	public FutUrls getFutUrls() {
		return futUrls;
	}

	public class FutUrls {
		String fut_start = "https://utas.s2.fut.ea.com/ut/game/fifa14/"; // Ps3

		/**
		 * Remove card from tradepile.
		 */
		String tradeInfo = fut_start + "trade";

		/**
		 * Used to bid on items
		 */
		String postBid = fut_start + "trade";

		/**
		 * Returns the watchlist, Also Remove card from watchlist
		 */
		String watchlist = fut_start + "watchlist";

		/**
		 * Get the credits, Can be used in keepAlive
		 */
		String credits = fut_start + "user/credits";
		/**
		 * Returns the unnsaigned items
		 */
		String unassigned = fut_start + "purchased/items";
		/**
		 * Too sell an item
		 */
		String searchAuctionsListItem = fut_start + "auctionhouse";
		
		/**
		 * Too relist an item?
		 */
		String searchAuctionsReListItem = fut_start + "auctionhouse/relist";

		/**
		 * Used for sendToTradePile
		 */
		String item = fut_start + "item";
		String user = fut_start + "user";

		/**
		 * Returns the tradepile
		 */
		String tradepile = fut_start + "tradepile";

		
		String pileSize = fut_start + "clientdata/pileSize";
		String searchAuctions = fut_start + "transfermarket";
		String tradeStatus = fut_start + "trade/status";
		
		String club = fut_start + "club";
		String clubItems = fut_start + "clubItemIds";
		String itemResource = fut_start + "item/resource";
		// String tradepile = fut_start + "tradepile";
		// String tradepile = fut_start + "tradepile";

		String authentication = "http://www.easports.com/iframe/fut/p/ut/auth";

	}

	/*
	 * <!-- ut api --> <authentication>auth</authentication>
	 * <PileSize>clientdata/pileSize</PileSize>
	 * <ServerConfig>settings</ServerConfig> <user>user</user>
	 * <Squad>squad</Squad> <SquadClone>squad/clone</SquadClone>
	 * <SquadList>squad/list</SquadList>
	 * <stickerBookSearch>stickerBookSearch</stickerBookSearch>
	 * <Credits>user/credits</Credits> <WatchList>watchlist</WatchList>
	 * <Watch>watchlist</Watch> <PostBid>trade</PostBid>
	 * <Unassigned>purchased/items</Unassigned>
	 * <SearchAuctionsListItem>auctionhouse</SearchAuctionsListItem>
	 * <SearchAuctionsReListItem>auctionhouse/relist</SearchAuctionsReListItem>
	 * <SearchAuctions>transfermarket</SearchAuctions>
	 * <TradeInfo>trade</TradeInfo> <TradeStatus>trade/status</TradeStatus>
	 * <Item>item</Item> <ItemResource>item/resource</ItemResource>
	 * <TradeTokens>user/bidTokens</TradeTokens>
	 * <TradePile>tradepile</TradePile> <Club>club</Club>
	 * <ClubConsumableSearch>club/consumables</ClubConsumableSearch>
	 * <ClubStaffStats>club/stats/staff</ClubStaffStats>
	 * <ClubConsumableStats>club/stats/consumables</ClubConsumableStats>
	 * <ClubItems>clubItemIds</ClubItems>
	 * <BidTokenPurchaseGroup>store/purchaseGroup/token</BidTokenPurchaseGroup>
	 * <AllPurchaseGroup>store/purchaseGroup/all</AllPurchaseGroup>
	 * <CardPackPurchaseGroup
	 * >store/purchaseGroup/cardpack</CardPackPurchaseGroup>
	 * <Transaction>store/transaction</Transaction>
	 * <LockboxToken>auth/nuc/token</LockboxToken>
	 * <EntitlementList>purchased/entitlements</EntitlementList>
	 * <ActiveMessage>activeMessage</ActiveMessage> <Stats>utStats</Stats>
	 * <TradeFeed>eventfeed</TradeFeed>
	 * <LeaderboardOptions>leaderboards/options</LeaderboardOptions>
	 * <LeaderboardEntry>leaderboards/period</LeaderboardEntry>
	 * <LeaderboardList>leaderboards</LeaderboardList>
	 * <MatchReset>match/reset</MatchReset>
	 */

}
