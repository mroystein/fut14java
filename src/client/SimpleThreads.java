package client;

public class SimpleThreads {

	// Display a message, preceded by
	// the name of the current thread
	static void threadMessage(String message) {
		String threadName = Thread.currentThread().getName();
		System.out.format("%s: %s%n", threadName, message);
	}

	private static class MessageLoop implements Runnable {
		String name;
		public MessageLoop(String name) {
			this.name=name;
		}
		public void run() {
			try {
				for (int i = 0; i < 100; i++) {
					// Pause for 4 seconds
					Thread.sleep(1000);
					// Print a message
					threadMessage("i="+i+" by "+name);
				}
			} catch (InterruptedException e) {
				threadMessage("I wasn't done!");
			}
		}
	}

	public static void main(String args[]) throws InterruptedException {

		// Delay, in milliseconds before
		// we interrupt MessageLoop
		// thread (default one hour).
		long patience = 1000 * 60 * 60;

		

		threadMessage("Bots have started");
		long startTime = System.currentTimeMillis();
		Thread t = new Thread(new MessageLoop("Kuken"));
		t.start();
		
		
		Thread t2 = new Thread(new MessageLoop("Horen"));
		t2.start();

		threadMessage("Waiting for MessageLoop thread to finish");
		// loop until MessageLoop
		// thread exits
		while (t.isAlive()) {
			threadMessage("Still waiting...");
			// Wait maximum of 1 second
			// for MessageLoop thread
			// to finish.
			t.join(1000);
			if (((System.currentTimeMillis() - startTime) > patience) && t.isAlive()) {
				threadMessage("Tired of waiting!");
				t.interrupt();
				// Shouldn't be long now
				// -- wait indefinitely
				t.join();
			}
		}
		threadMessage("Stopped!");
	}
}