package client;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import jsonEntities.AccountInfoGson;
import jsonEntities.AuctionInfoGson;
import jsonEntities.CreditGson;
import jsonEntities.Fut_UserGson;
import jsonEntities.AccountInfoGson.UserAccountInfo.PersonGson;
import jsonEntities.ItemGson;
import jsonEntities.ItemGson.ItemData;

import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.message.HeaderGroup;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.gson.Gson;

public class Core {

	private String email, password, secret_answer, nucleus_id;
	private int credits;
	private Urls urls;
	private PersonGson personJson; // Account name, club name etc
	private CloseableHttpClient httpclient;
	private HeaderGroup hg;
	private HttpClientContext context;

	private Gson gson;

	public Core(String email, String password, String secret_answer) {
		this.email = email;
		this.password = password;
		this.secret_answer = secret_answer;
		this.credits = 0;
		urls = new Urls();
		gson = new Gson();
		init();

	}
	
	private void init(){
		hg = new HeaderGroup();

		RequestConfig globalConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.BEST_MATCH).build();

		CookieStore cookieStore = new BasicCookieStore();

		context = HttpClientContext.create();
		context.setCookieStore(cookieStore);

		// HttpClientBuilder builder = HttpClients.custom();
		httpclient = HttpClients.custom().setDefaultRequestConfig(globalConfig).setDefaultCookieStore(cookieStore)
				.setRedirectStrategy(new LaxRedirectStrategy()).build();
	}

	// Get the url for login
	private void login_getLoginUrl() {
		HttpGet httpget = new HttpGet(urls.fut_home);
		httpget.setHeaders(hg.getAllHeaders());
		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpget, context);

			HttpHost target = context.getTargetHost();
			List<URI> redirectLocations = context.getRedirectLocations();
			URI location = URIUtils.resolve(httpget.getURI(), target, redirectLocations);
			urls.setLoginUrl(location.toASCIIString());
			// System.out.println("Login url found:" + urls.login);

		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} finally {
			try {
				response.close();
			} catch (IOException e) { // TODO
				e.printStackTrace();
			}

		}
	}

	private Collection<BasicHeader> setDefaultHeaders() {
		Collection<BasicHeader> coll = new LinkedList<>();
		coll.add(new BasicHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:26.0) Gecko/20100101 Firefox/26.0"));
		coll.add(new BasicHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"));
		coll.add(new BasicHeader("Accept-Encoding", "gzip, deflate, sdch"));
		coll.add(new BasicHeader("Accept-Language", "en-US,en;q=0.8"));
		coll.add(new BasicHeader("Connection", "keep-alive"));
		coll.add(new BasicHeader("DNT", "1"));
		for (BasicHeader bh : coll) {
			hg.updateHeader(bh);
		}

		return coll;
	}

	// FOR ACC INFO
	private Collection<BasicHeader> updateHeadersForAccInfo() {
		Collection<BasicHeader> coll = new LinkedList<>();
		coll.add(new BasicHeader("Content-Type", "application/json"));
		coll.add(new BasicHeader("Accept", "text/json"));
		coll.add(new BasicHeader("Easw-Session-Data-Nucleus-Id", nucleus_id));
		coll.add(new BasicHeader("X-UT-Embed-Error", "true"));
		coll.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
		coll.add(new BasicHeader("X-UT-Route", urls.fut_host));
		coll.add(new BasicHeader("Referer", urls.futweb));
		for (BasicHeader bh : coll) {
			hg.updateHeader(bh);
		}
		if (!hg.containsHeader("Content-Type")) {
			System.err.println("WRONGGGG");
		}
		return coll;
	}

	// FOR authorization
	private Collection<BasicHeader> updateHeadersForAuth() {
		Collection<BasicHeader> coll = new LinkedList<>();
		coll.add(new BasicHeader("Accept", "application/json, text/javascript"));
		coll.add(new BasicHeader("Origin", "http://www.easports.com"));
		coll.add(new BasicHeader("Host", "www.easports.com"));

		for (BasicHeader bh : coll) {
			hg.updateHeader(bh);
		}
		return coll;
	}

	private void findEA_ID_Nucelus(String s) {
		int index = s.indexOf("EASW_ID");
		index = s.indexOf("'", index);
		int endIndex = s.indexOf("'", index + 1);
		nucleus_id = s.substring(index + 1, endIndex);
		// System.out.println("EASW_ID str : " + ea_id);
	}

	public void login(boolean debugOn) {
		setDefaultHeaders(); // Fills headerGroup
		// Get the login url by a HTTPGET
		login_getLoginUrl();

		// LOGIN - POST USER INFO
		if (debugOn)
			System.out.println("-------------  Posting user information - LOGIN ---------------");

		List<NameValuePair> formparams = new ArrayList<NameValuePair>();
		formparams.add(new BasicNameValuePair("email", email));
		formparams.add(new BasicNameValuePair("password", password));
		formparams.add(new BasicNameValuePair("_rememberMe", "on"));
		formparams.add(new BasicNameValuePair("rememberMe", "on"));
		formparams.add(new BasicNameValuePair("_eventId", "submit"));
		formparams.add(new BasicNameValuePair("facebookAuth", ""));
		UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);

		hg.updateHeader(new BasicHeader("Referer", urls.main_site));
		httpPost(urls.login, formEntity, false);

		// - Launch futweb
		if (debugOn)
			System.out.println("------------- Launce futweb, getting EASW_ID ---------------");
		hg.updateHeader(new BasicHeader("Referer", urls.fut_home));
		String rc = httpGet(urls.futweb, debugOn);

		findEA_ID_Nucelus(rc);

		// GET SHARDs ??

		// rc = httpGet(urls.shards + "" + System.currentTimeMillis());
		// System.out.println("------ --- Shards -------");
		// System.out.println(rc);
		// Update headers
		if (debugOn)
			System.out.println("------------- Getting accunt info ---------------");
		updateHeadersForAccInfo();
		String acc_info = "http://www.easports.com/iframe/fut/p/ut/game/fifa14/user/accountinfo?_=" + System.currentTimeMillis();

		rc = httpGet(acc_info, debugOn);
		AccountInfoGson acc = gson.fromJson(rc, AccountInfoGson.class);
		personJson = acc.getPerson();

		System.out.println(personJson);
		if (debugOn)
			System.out.println("------------- authorization POST ---------------");

		updateHeadersForAuth();
		StringEntity authParameters = getSidParams();

		rc = httpPost(urls.getFutUrls().authentication, authParameters, debugOn);
		Map map = gson.fromJson(rc, Map.class);
		String sid = (String) map.get("sid");
		if (debugOn) {
			System.out.println("Auth Paras: " + authParameters.toString());

			System.out.println("SID RC :" + rc);
			System.out.println("Sid:" + sid);
		}

		hg.addHeader(new BasicHeader("X-UT-SID", sid));
		hg.updateHeader(new BasicHeader("Accept", "text/json"));
		hg.updateHeader(new BasicHeader(
				"Referer",
				"http://www.easports.com/iframe/fut/?baseShowoffUrl=http%3A%2F%2Fwww.easports.com%2Fuk%2Ffifa%2Ffootball-club%2Fultimate-team%2Fshow-off&guest_app_uri=http%3A%2F%2Fwww.easports.com%2Fuk%2Ffifa%2Ffootball-club%2Fultimate-team&locale=en_GB"));
		removeHeaderIfExists("Origin");
		// removeHeaderIfExists("Content-Type");

		if (debugOn)
			System.out.println("------------- Validate Secret Question GET ---------------");
		// printHeaders(hg.getAllHeaders());
		String url = urls.fut_question + "" + System.currentTimeMillis();
		rc = httpGet(url, debugOn);
		// {"question":1,"attempts":5,"recoverAttempts":20}
		// or contains Already answered question.
		if (!rc.contains("Already answered question")) {
			// Need to validate secret question hash
			hg.updateHeader(new BasicHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"));
			rc = httpPost(urls.fut_validate, makeSecretHashEntity(), false);
			hg.updateHeader(new BasicHeader("Content-Type", "application/json")); // Update
																					// back

		}
		if (debugOn)
			System.out.println(rc);
		map = gson.fromJson(rc, Map.class);

		String token = (String) map.get("token");
		if (debugOn)
			System.out.println("token:" + token);

		hg.updateHeader(new BasicHeader("X-UT-PHISHING-TOKEN", token));

		// Update headers for Ultimate Team
		removeHeaderIfExists("Easw-Session-Data-Nucleus-Id");
		removeHeaderIfExists("X-Requested-With");
		removeHeaderIfExists("X-UT-Route");
		hg.updateHeader(new BasicHeader("Referer", "http://www.easports.com/iframe/fut/bundles/futweb/web/flash/FifaUltimateTeam.swf"));
		hg.updateHeader(new BasicHeader("Origin", "http://www.easports.com"));
		hg.updateHeader(new BasicHeader("Accept", "application/json"));

		if (debugOn)
			System.out.println("------------- User FUT GET ---------------");

		rc = httpGet(urls.futUrls.user, debugOn);
		Fut_UserGson u = gson.fromJson(rc, Fut_UserGson.class);

		System.out.println("Coins: " + u.getCredits());
		credits=u.getCredits();

		System.out.println("Login successfull!");

	}

	private HttpEntity makeSecretHashEntity() {
		EAHashAlg eaHash = new EAHashAlg();
		String hash = eaHash.EAHash(secret_answer);
		return new StringEntity("answer=" + hash, Charset.forName("UTF-8"));

	}

	private void removeHeaderIfExists(String headerName) {
		if (hg.containsHeader(headerName)) {// Remove Origin header
			Header hRemove = hg.getFirstHeader(headerName);
			hg.removeHeader(hRemove);
		}
	}

	private StringEntity getSidParams() {
		String paraStr = String
				.format("{ \"isReadOnly\": false, \"sku\": \"FUT14WEB\", \"clientVersion\": 1, \"nuc\": %s, \"nucleusPersonaId\": %s, \"nucleusPersonaDisplayName\": \"%s\", \"nucleusPersonaPlatform\": \"ps3\", \"locale\": \"en-GB\", \"method\": \"authcode\", \"priorityLevel\":4, \"identification\": { \"authCode\": \"\" } }",
						nucleus_id, personJson.getPersonaId(), personJson.getName());
		// System.out.println(paraStr);

		return new StringEntity(paraStr, Charset.forName("UTF-8"));

	}

	public void printContent(HttpEntity entity) {
		try {
			InputStream is = entity.getContent();

			BufferedReader in = new BufferedReader(new InputStreamReader(is));

			String inputLine;
			while ((inputLine = in.readLine()) != null)
				System.out.println(inputLine);
			in.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void printHeaders(Header[] headers) {
		System.out.println("Headers size :" + headers.length);
		for (int i = 0; i < headers.length; i++) {
			System.out.println("\t" + headers[i].getName() + " : " + headers[i].getValue());
		}
	}

	private String httpPost(String url, HttpEntity formEntity, boolean debugOn) {

		CloseableHttpResponse response = httpPostResp(url, formEntity, debugOn);
		HttpEntity entity = response.getEntity();
		String responseStr = "";
		try {
		
			responseStr = EntityUtils.toString(entity, Charset.forName("UTF-8"));
			if (debugOn)
				System.out.println(responseStr);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}
		try {
			response.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return responseStr;

	}

	private CloseableHttpResponse httpPostResp(String url, HttpEntity formEntity, boolean debugOn) {
		HttpPost httppost = new HttpPost(url);

		String debugInf = String.format("HTTP Post %s ", url);
		if (debugOn)
			System.out.println(debugInf);

		if (formEntity != null) {
			httppost.setEntity(formEntity);
		}

		httppost.setHeaders(hg.getAllHeaders());

		CloseableHttpResponse response = null;

		if (debugOn)
			printHeaders(httppost.getAllHeaders());
		try {
			response = httpclient.execute(httppost, context);

			if (debugOn) {
				System.out.println("StatusCode: " + response.getStatusLine().getStatusCode());
				printCookies();
			}

			return response;

		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	private void printCookies() {
		System.out.println(" ------- COOKIES ------------");
		for (Cookie c : context.getCookieStore().getCookies()) {
			System.out.println("\t" + c.getName() + " : " + c.getValue());
		}
	}

	private String httpGet(String url, boolean debugOn) {
		HttpGet httpget = new HttpGet(url);
		String debugInf = String.format("HTTP GET %s ", url);
		if (debugOn)
			System.out.println(debugInf);

		httpget.setHeaders(hg.getAllHeaders());

		CloseableHttpResponse response = null;
		try {
			response = httpclient.execute(httpget, context);
			// printHeaders(httpget.getAllHeaders());
			// printCookies();
			if (debugOn)
				System.out.println(response.getStatusLine());

			HttpEntity entity = response.getEntity();
			return EntityUtils.toString(entity);

		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "";
	}

	// ---------------- FUT Methods ---------------------

	public ArrayList<ItemGson> getTradePile() {
		String rc = httpGet(urls.futUrls.tradepile, false);
		return handleErrosInAuctionInfoParsing(rc);
	}

	public ArrayList<ItemGson> getWatchList() {
		String rc = httpGet(urls.futUrls.watchlist, false);
		return handleErrosInAuctionInfoParsing(rc);
	}
	
	public void setCredits(int credits){
		this.credits=credits;
	}
	
	public ArrayList<ItemGson> handleErrosInAuctionInfoParsing(String rc){
		AuctionInfoGson ahInfo = gson.fromJson(rc, AuctionInfoGson.class);
		if(ahInfo!=null){
			ArrayList<ItemGson> items = ahInfo.getAuctionInfo();
			if(items!=null){
				setCredits(ahInfo.getCredits()); // Update credits
				return items;
			}
			
			
		}
		System.err.println("Error, rc: "+rc);
		if(rc.contains("expired session")){
			System.err.println("Session Expired. Will try to relog in 1 min");
			try {
			    Thread.sleep(60000);
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			}
			init();
			login(false);
		}
		
		
		return new ArrayList<ItemGson>(); // Return empty list
	}

	public ArrayList<ItemGson> getUnnasigned() {
		String rc = httpGet(urls.futUrls.unassigned, false);
		return handleErrosInAuctionInfoParsing(rc);
	}

	/**
	 * 
	 * @param itemId
	 *            itemId, and NOT trade Id
	 * @param startingBid
	 * @param buy_now
	 * @param duration
	 *            3600 = 1 hour
	 */
	public void sell(long itemId, int startingBid, int buy_now, int duration) {
		// {buyNowPrice:0, startingBid:150, duration:3600, itemData:{id=10}}
		System.out.println("Selling");
		String data2 = String
				.format("{\"duration\":%s,\"startingBid\":%s,\"buyNowPrice\":%s,\"itemData\":{\"id\":%s}}", duration, startingBid, buy_now, itemId);
		StringEntity se = new StringEntity(data2, Charset.forName("UTF-8"));
		printContent(se);
		String rc = httpPost(urls.futUrls.searchAuctionsListItem, se, false);

		System.out.println(rc);

		// Response : {"id":139243080758} , tradeId
		/*
		 * try{ Map<String, Long> m = gson.fromJson(rc, Map.class); Double
		 * tradeId = m.get("id"); System.out.println("TradeId="+tradeId);
		 * }catch(Exception e){ System.err.println("Error in Sell(). RC :"+rc);
		 * e.printStackTrace(); }
		 */

	}

	/**
	 * Also updates credits.
	 */
	public void keepAlive() {
		String rc = httpGet(urls.futUrls.credits, false);

		credits = gson.fromJson(rc, CreditGson.class).getCredits();
		System.out.println("Updated credits: " + credits);

	}

	/**
	 * Removes a card from watchlist
	 * 
	 * @param tradeId
	 */
	public void watchlistDelete(long tradeId) {
		//System.out.println("WatchlistDelete() tradeId =" + tradeId);
		String data = String.format("{tradeId:%s}", tradeId);
		data = String.format("tradeId=%s", tradeId);
		StringEntity se = new StringEntity(data, Charset.forName("UTF-8"));
		// X-HTTP-Method-Override:DELETE
		//printContent(se);
		hg.updateHeader(new BasicHeader("X-HTTP-Method-Override", "DELETE"));
		String newUrl = urls.futUrls.watchlist + "?tradeId=" + tradeId;
		String rc = httpPost(newUrl, se, false);
		removeHeaderIfExists("X-HTTP-Method-Override");
		// System.out.println(rc);
	}

	/**
	 * Removes a card from tradePileDelete
	 * 
	 * @param tradeId
	 */
	public void tradePileDelete(long tradeId) {
		System.out.println("tradePileDelete() tradeId =" + tradeId);

		hg.updateHeader(new BasicHeader("X-HTTP-Method-Override", "DELETE"));
		String newUrl = urls.futUrls.tradeInfo + "/" + tradeId;
		String rc = httpPost(newUrl, null, false);
		System.out.println(rc);
		removeHeaderIfExists("X-HTTP-Method-Override");
		// System.out.println(rc);
	}

	/**
	 * Send item to tradepile. Can do it with tradeId also (Not implemented yet)
	 * 
	 * @param itemId
	 */
	public void sendToTradepile(long itemId, long tradeId) {
		// {"itemData":[{"pile":"trade","id":"105722923181"}]}
		// Do a post
		String data = "";
		if (tradeId < 0) {
			data = "{\"itemData\":[{\"tradeId\":" + tradeId + ",\"pile\":\"trade\",\"id\":\"" + itemId + "\"}]}";
		} else {
			data = "{\"itemData\":[{\"pile\":\"trade\",\"id\":\"" + itemId + "\"}]}";
		}

		// X-HTTP-Method-Override
		hg.updateHeader(new BasicHeader("X-HTTP-Method-Override", "PUT"));
		StringEntity ent = new StringEntity(data, Charset.forName("UTF-8"));
		String rc = httpPost(urls.futUrls.item, ent, false);
		// System.out.println(rc);
		removeHeaderIfExists("X-HTTP-Method-Override");

	}
	
	/**
	 * Updated sometimes.
	 * Atleast after a BID() it is updated
	 * @return
	 */
	public int getCredits(){
		return credits;
	}
	
	public boolean bid(long tradeId, long itemId,int bid){
		
		//X-HTTP-Method-Override:PUT
		// Might get {"debug":"","string":"Permission Denied","reason":"","code":"461"}
		String url = urls.futUrls.postBid+"/"+tradeId+"/bid";
		String data ="{\"bid\":"+bid+"}";
		StringEntity ent = new StringEntity(data, Charset.forName("UTF-8"));
		
		//System.out.println("Data : "+data);
		hg.addHeader(new BasicHeader("X-HTTP-Method-Override", "PUT"));
		String rc = httpPost(url, ent, false);
		removeHeaderIfExists("X-HTTP-Method-Override");
		AuctionInfoGson ahInfo = gson.fromJson(rc, AuctionInfoGson.class);
		if(ahInfo==null || ahInfo.getAuctionInfo()==null || ahInfo.getAuctionInfo().get(0)==null){
			System.err.println("Error in Bid()");
			System.err.println(rc);
			return false;
		}
		credits=ahInfo.getCredits();
		ItemGson item = ahInfo.getAuctionInfo().get(0);
		//System.out.println("Bidded on : "+item);
		if(item.getBidState().equals("highest") ||item.getBidState().equals("buyNow") && item.getTradeState().equals("closed")){
			return true;
		}else{
			return false;
		}
		
		
	}
	
	
	private int pageSize = 16;
	/**
	 * 
	 * @param category (playStyle
	 * @param type training, development
	 * @param level bronze, silver, gold
	 * @param playStyle 266=Hunter etc
	 * @param start currentPage
	 * @param maxBid maxBid on the search
	 * @return
	 */
	public ArrayList<ItemGson> searchAuctions(String category,String type, String level, String playStyle, int start, int maxBid,int maxBuy,long maskedDefId) {
		int minBid = 0;
		int minBuy = 0;
		
		// int start = 0; // Current page, increases by +=pageSize
		
		
		// 1615613742 ResourceId GoldContractPlayer

		List<NameValuePair> data = new ArrayList<NameValuePair>();
		data.add(new BasicNameValuePair("num", pageSize + ""));
		data.add(new BasicNameValuePair("start", start + ""));
		
		if(category!=null)
			data.add(new BasicNameValuePair("cat", category));
		if(level!=null)
			data.add(new BasicNameValuePair("lev", level));
		if (maskedDefId > 0)
			data.add(new BasicNameValuePair("maskedDefId", maskedDefId + ""));
		if (maxBid > 0)
			data.add(new BasicNameValuePair("macr", maxBid + ""));
		if (maxBuy > 0)
			data.add(new BasicNameValuePair("maxb", maxBuy + ""));
		if (minBid > 0)
			data.add(new BasicNameValuePair("micr", minBid + ""));
		if (minBuy > 0)
			data.add(new BasicNameValuePair("minb", minBuy + ""));
		if(playStyle!=null)
			data.add(new BasicNameValuePair("playStyle", playStyle));
		
		
		if(type!=null)
			data.add(new BasicNameValuePair("type", type));
	
		try {
			URIBuilder builder = new URIBuilder(urls.futUrls.searchAuctions);
			builder.addParameters(data);
			String newUrl = builder.build().toString();
			//System.out.println("------------------------------------");
			System.out.println(newUrl);
			//String debugStr = String.format("p=%s, ",  start);
			//System.out.print("p=");
			//String rc = httpPost(url, formEntity, true);
			
			
			String rc = httpGet(newUrl, false);

		
			return handleErrosInAuctionInfoParsing(rc);

		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		return null;

	}

	public ArrayList<ItemGson> searchAuctions_Contract(int start, int maxBid) {
		String category = SearchVar.Category_Contract;
		String level = SearchVar.Level_Gold;
		String type = SearchVar.Type_Development;
		
		return searchAuctions(category, type, level,null, start, maxBid,0,0);

	}

	/**
	 * @param args
	 */
	public static void runTest() {
		Core core = new Core("", "", "");

		// Core core = new Core("mr_oystein@hotmail.com", "giefepix1Origin",
		// "klovning");
		core.login(true);

		// core.sell(10, 150, 0, 3600);

		ArrayList<ItemGson> tradepile = core.getTradePile();
		System.out.println("Tradepile size :" + tradepile.size());
		for (ItemGson item : tradepile) {
			System.out.println(item);
			long itemId = item.getItemData().getId();
			long tradeId = item.getTradeId();
			String tradeState = item.getTradeState();
			String bidState = item.getBidState();
			int currentBid = item.getCurrentBid();
			if (tradeState.equals("None")) {
				// Item was just bought and won, need to list it on market
				String s = "\t Item was just bought and won, need to list it on market";
				System.out.println(s);

			} else if (tradeState.equals("expired") && currentBid == 0) {
				// Item has expired, and wasnt sold. Need to relist
				String s = "\t Item has expired, and wasnt sold. Need to relist";
				System.out.println(s);

				core.sell(itemId, 500, 550, 3600);

				try {
					Thread.sleep(5000); // 1 sec
				} catch (InterruptedException ex) {
					Thread.currentThread().interrupt();
				}

			} else if (tradeState.equals("closed")) {
				// Item has sold
				String s = "\t Sold item for " + item.getCurrentBid() + " coins.";
				System.out.println(s);
				core.tradePileDelete(tradeId);
			}

		}

		ArrayList<ItemGson> watchlist = core.getWatchList();
		System.out.println("watchlist size :" + watchlist.size());
		for (ItemGson item : watchlist) {

			System.out.println(item);
			// ItemData itemData = item.getItemData();
			long tradeId = item.getTradeId();
			String tradeState = item.getTradeState();
			String bidState = item.getBidState();
			if (bidState.equals("outbid") || (bidState.equals("none") && tradeState.equals("expired"))) {
				core.watchlistDelete(tradeId);
				System.out.println("Removed item from watchlist");
			} else if (bidState.equals("highest") && tradeState.equals("closed")) { // Won
																					// it

			}

		}
		System.out.println("Keeep Alive ---------");
		core.keepAlive();

	}

	public static void main(String[] args) {
		System.out.println("Startet");
		// runTest();
		Core core = new Core("", "", "");
		// core.login();
		long itemId = 105722923181L;
		String data = "{\"itemData\":[{\"pile\":\"trade\",\"id\":\"" + itemId + "\"}]}";
		StringEntity ent = new StringEntity(data, Charset.forName("UTF-8"));
		core.printContent(ent);

		/*
		 * System.out.println("Auction search results");
		 * 
		 * int start =0; ArrayList<ItemGson> list = core.searchAuctions(start,
		 * 350); for(ItemGson item: list){ System.out.println(item); }
		 */

		/*
		 * Gson gson = new Gson(); String json = "{\"id\":139246546349}";
		 * Map<String, Long> map = gson.fromJson(json, Map.class); Long l =
		 * map.get("id"); System.out.println(l);
		 */

	}

}
