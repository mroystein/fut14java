package client;

import helperClasses.Accounts;
import helperClasses.Accounts.Account;
import helperClasses.SearchItem;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import db.FutDB;

import jsonEntities.ItemGson;

public class FutWorker implements Runnable {
	private Core core;

	public static int sleep_TwoSec = 2000;
	public static int sleep_30Sec = 30000;
	public static int sleep_OneMin = 60000;
	public static int sleep_TwoMin = 120000;

	private ArrayList<SearchItem> cards;
	private Accounts accs;

	// private Account myAcc;
	private String logFile;
	private FutDB db ;

	public FutWorker(Account myAcc, ArrayList<SearchItem> items) {
		// this.myAcc = myAcc;

		logFile = "log_" + myAcc.getClubName() + ".txt";
		cards = items;
		

		//DB stuff
		db = new FutDB();
		System.out.println(db.getInventory().size() +" items in inverntoy db");
		for(SearchItem it : items){
			SearchItem temp = db.getPlayerFromInventory(it.getMaskedDefId());
			if(temp==null){
				db.insertInventoryPlayer(it);
			}
		}
		//cards = db.getInventory();
		
		//DB END

		accs = new Accounts();

		printStatus("FutMain start");

		core = new Core(myAcc.getEmail(), myAcc.getPassword(), myAcc.getQuestion());
		//core.login(false);
		
		
		
	

	}

	public void printStatus(String s) {
		/*
		 * if(jTextArea!=null){ jTextArea.append(s+"\n"); }
		 */
		System.out.println(s);
	}

	public void runBot() {

	}

	private void updatePrice(SearchItem c) {
		/*
		 * MaxBid = 0.8 of cheapestBuyNow SellStart = nextBid(maxBid) //Will
		 * always earn SellEnd = nextBid(cheapestBuyNow )
		 */
		System.out.println("Current : " + c + ", Profit: " + c.getProfit());
		int cheapestBN = getCheapestBNBinary(c);

		double newBidTemp = cheapestBN * 0.8;
		double newSellStart = cheapestBN * 0.95; // 10% profit
		double newSellEnd = cheapestBN * 1.1; // 34 % profit

		int newBid = (int) newBidTemp;
		newBid = findNearestValidBid(newBid);

		int sellStart = findNearestValidBidDouble(newSellStart);
		int sellEnd = findNearestValidBidDouble(newSellEnd);

		c.setMaxBuyNow(cheapestBN);
		c.setMaxBid(newBid);
		c.setSellStart(sellStart);
		c.setSellBuyNow(sellEnd);

		if (c.getSellStart() * 0.95 <= c.getMaxBid()) {
			System.err.println("Card has minus profit!! WTF  " + c);
			c.setBuyMore(false);
		}

		System.out.println("Updated : " + c + ", Profit: " + c.getProfit());

		// sellStart, sellBuyNow,maxBid

	}

	@Override
	public void run() {
		/*
		 * for(SearchItem c: cards){ getCheapestBNBinary(c); }
		 */

		Random r = new Random();
		mySleep(r.nextInt(sleep_30Sec));
		boolean b = true;
		while (b) {
			if (isMoreRoomToBuy()) {
				searchAndBuy();
			} else {
				System.out.println("Watch/Tradepile is full. Sleeping more");
				mySleep(r.nextInt(sleep_OneMin));
			}

			mySleep(r.nextInt(sleep_30Sec));
			core.keepAlive();
			mySleep(2000);
			reListAndMove();
			mySleep(r.nextInt(sleep_OneMin));
			core.keepAlive();
			mySleep(r.nextInt(sleep_OneMin));
		}

		//
		// core.
		// core.keepAlive();

	}
	long lastPriceCheckTime =0;

	public void searchAndBuy() {
		long oneHour = 3600000;
		if(lastPriceCheckTime<System.currentTimeMillis()){
			System.out.println("----------- Time to update prices!! ------------ ");
			for (SearchItem c : cards) {
				if (c.isBuyMore()) {
					updatePrice(c);
				}
			}
			lastPriceCheckTime=oneHour+System.currentTimeMillis();
		}
		Random r = new Random();
		for (SearchItem c : cards) {
			if (c.isBuyMore()) {
				mySleep(r.nextInt(sleep_TwoSec));
				searchAndBuy(c);
				mySleep(r.nextInt(sleep_TwoSec));

			}
		}
	}

	public boolean isMoreRoomToBuy() {

		mySleep(333);
		int tradePileSize = core.getTradePile().size();

		mySleep(333);
		int watchListSize = core.getWatchList().size();
		mySleep(333);
		int tradeWatchSize = tradePileSize + watchListSize;
		return tradeWatchSize < 29;
	}

	public void searchAndBuy(SearchItem card) {
		Random r = new Random();

		if (card.getMaxBid() >= core.getCredits()) {
			printStatus("Not enough money. No point in searching for " + card.getName());
			return;
		}

		mySleep(r.nextInt(666));
		int tradePileSize = core.getTradePile().size();
		mySleep(r.nextInt(666));
		int watchListSize = core.getWatchList().size();
		printStatus("Watchlist size:" + watchListSize);
		int tadeWatchSize = tradePileSize + watchListSize;
		mySleep(r.nextInt(666));
		String s = String.format("Searching for %s | TradePile %s | WatchPile %s ", card.getName(), tradePileSize, watchListSize);
		printStatus(s);

		int currPage = 0; // Current page, increse by 16
		boolean keepSearching = true;

		while (keepSearching) {
			int searchMaxBid = calculatePrevBid(card.getMaxBid());
			int maxBuy = 0; //Only looking at bidding...
			ArrayList<ItemGson> items = core.searchAuctions(card.getCategory(), card.getType(), card.getLevel(), card.getPlayStyle(), currPage, searchMaxBid,
					maxBuy, card.getMaskedDefId());
			for (ItemGson item : items) {

				if (item.getItemData().getResourceId() != card.getResourceId()) {
					System.err.println("ID's are wrong!!");
				}

				if (tadeWatchSize > 29) {
					printStatus("Trade/Watchpile is full. Stopping searching");
					return;
				}
				if (accs.isMyAccount(item.getSellerName())) {
					printStatus("Dont bid, highest bidder is own account");
					return;
				}
				if (item.getItemData().getResourceId() != card.getResourceId()) {
					System.err.println("Wrong card data.. fix this shit");
				}

				int bid = calculateNextBid(item);
				if (bid > core.getCredits()) {
					printStatus("Not enough money. Stopping seraching..");
					return;
				}
				if (item.getExpires() > 300) {// Min 5 min left (5*60=300)
					printStatus("No point on bidding, so long time left");
					return;
				}

				if (bid <= card.getMaxBid()) {
					printStatus("Bidding " + bid + " on " + item);
					boolean success = core.bid(item.getTradeId(), item.getItemData().getId(), bid);
					printStatus("Success :" + success);
					if (success) {
						tadeWatchSize++;

					}

					mySleep(r.nextInt(666));

				}
				// mySleep(500);
				// System.out.println(item);
			}

			currPage += 16;
			mySleep(r.nextInt(5000));// 5 sec
			if (items.size() < 16) {
				keepSearching = false;
				printStatus("Finished searching.. No more items");
			}
		}
	}

	public void reListAndMove() {
		mySleep(500);
		ArrayList<ItemGson> tradepile = core.getTradePile();
		mySleep(500);
		ArrayList<ItemGson> watchlist = core.getWatchList();
		if (watchlist == null || tradepile == null) {
			System.err.println("tradepile/watchpile is null");
			return;
		}
		printStatus("watchlist size :" + watchlist.size());
		// ------------ WatchList --------------------
		int removedItems = 0;

		for (ItemGson item : watchlist) {

			// System.out.println(item);

			mySleep(1000);

			long tradeId = item.getTradeId();
			String tradeState = item.getTradeState();
			String bidState = item.getBidState();
			long itemId = item.getItemData().getId();

			SearchItem card = getSearchItem(item.getItemData().getResourceId());

			if (bidState.equals("outbid") || (bidState.equals("none") && tradeState.equals("expired"))) {
				core.watchlistDelete(tradeId);
				removedItems++;

			} else if (bidState.equals("highest") && tradeState.equals("closed")) {
				// Won it

				core.sendToTradepile(itemId, tradeId);
				printStatus("Sendt item to tradepile");

				String s = "\t Won item '" + card.getName() + "' for " + item.getCurrentBid() + " coins.";

				logToFile(s);

			} else {
				// Do nothing. Currently a bid on it..
			}

		}

		if (removedItems > 0) {
			System.out.println("Removed " + removedItems + " from watchlist (outbid)");
		}

		// ------------ Tradepile --------------------
		printStatus("Tradepile size :" + tradepile.size());
		for (ItemGson item : tradepile) {
			// System.out.println(item);
			long itemId = item.getItemData().getId();
			long tradeId = item.getTradeId();
			String tradeState = item.getTradeState();
			String bidState = item.getBidState();
			int currentBid = item.getCurrentBid();

			SearchItem card = getSearchItem(item.getItemData().getResourceId());
			if (card == null) {
				System.err.println("Card isnt in stock, dunno how to sell");
				return;
			}

			mySleep(500);

			if (tradeState == null || tradeState.equals("None")) {
				// Item was just bought and won, need to list it on market
				String s = "\t Item '" + card.getName() + "' was just bought and won, need to list it on market";
				printStatus(s);

				core.sell(itemId, card.getSellStart(), card.getSellBuyNow(), 3600);

			} else if (tradeState.equals("expired") && currentBid == 0) {
				// Item has expired, and wasnt sold. Need to relist
				String s = "\t Item '" + card.getName() + "' has expired, and wasnt sold. Need to relist";
				printStatus(s);

				core.sell(itemId, card.getSellStart(), card.getSellBuyNow(), 3600);

			} else if (tradeState.equals("closed")) {
				// Item has sold
				String s = "\t Sold item '" + card.getName() + "' for " + item.getCurrentBid() + " coins.";
				printStatus(s);
				logToFile(s);

				core.tradePileDelete(tradeId);
			}

		}
	}

	public HashMap<Integer, Integer> getPricesMap(SearchItem card) {
		Random r = new Random();
		printStatus("Searching for BIN prices for " + card.getName());
		mySleep(333);

		// Price, Count
		HashMap<Integer, Integer> map = new HashMap<>();
		int cheapestBN = Integer.MAX_VALUE; // cheapestBuyNow
		// If there is possible to buy the card with 2 bids higher than my bid,
		// then stop buying it!
		int maxBuyNow = 0;
		int searchMaxBid = 0;
		System.out.println("Validating card " + card.getName() + ", seeing if we can buyNow for " + maxBuyNow + " , myBid prices is " + card.getMaxBid());

		int currPage = 0; // Current page, increse by 16
		ArrayList<ItemGson> items = null;

		do {
			items = core.searchAuctions(card.getCategory(), card.getType(), card.getLevel(), card.getPlayStyle(), currPage, searchMaxBid, maxBuyNow,
					card.getMaskedDefId());
			currPage += 16;
			for (ItemGson item : items) {

				Integer count = map.get(item.getBuyNowPrice());
				if (item.getBuyNowPrice() > 0 && item.getBuyNowPrice() < cheapestBN) {
					cheapestBN = item.getBuyNowPrice();
				}

				if (count == null) {
					map.put(item.getBuyNowPrice(), 1);
				} else {
					map.put(item.getBuyNowPrice(), count + 1);
				}
			}
			mySleep(r.nextInt(1000));// 0 to 1 sec

		} while (items != null && items.size() > 1);

		Set<Integer> set = map.keySet();
		ArrayList<Integer> prices = new ArrayList<>(set);
		Collections.sort(prices);

		HashMap<Integer, Integer> newMap = new HashMap<>();
		for (int i = 1; i < 5; i++) {
			int mPrice = prices.get(i);
			int mCount = map.get(mPrice);
			newMap.put(mPrice, mCount);

		}
		System.out.println("HashMap : Cheapest BN for " + card.getName() + " is " + cheapestBN + " coins");
		// Collections.sort(set);
		return newMap;

	}

	public int getCheapestBNBinary(SearchItem card) {
		Random r = new Random();
		printStatus("Searching for BIN prices for " + card.getName());
		mySleep(333);

		int cheapestBN = Integer.MAX_VALUE; // cheapestBuyNow

		int maxBuyNow = card.getMaxBuyNow();// card.getMaxBid();
		if (maxBuyNow < 250) {
			maxBuyNow = card.getMaxBid();
		}
		int searchMaxBid = 0;
		// System.out.println("Checking price for " + card.getName() +
		// ", seeing if we can buyNow for " + maxBuyNow + " , myBid prices is "
		// + card.getMaxBid());

		ArrayList<ItemGson> items = null;
		boolean keepSearching = true;
		while (keepSearching) {
			items = core.searchAuctions(card.getCategory(), card.getType(), card.getLevel(), card.getPlayStyle(), 0, searchMaxBid, maxBuyNow,
					card.getMaskedDefId());
			// System.out.println("Item size: " + items.size());
			if (items.size() == 0) {
				mySleep(500); // Do another search.. just to be sure it's
								// nothing there...
				items = core.searchAuctions(card.getCategory(), card.getType(), card.getLevel(), card.getPlayStyle(), 0, searchMaxBid, maxBuyNow,
						card.getMaskedDefId());
			}

			for (ItemGson item : items) {
				if (item.getBuyNowPrice() > 0 && item.getBuyNowPrice() < cheapestBN) {
					cheapestBN = item.getBuyNowPrice();
				}

			}
			mySleep(r.nextInt(1000));// 0 to 1 sec

			if (items.size() > 0) { // items at that price
				maxBuyNow = calculatePrevBid(maxBuyNow);
			} else {
				// no items at that price. go up unless found one before
				maxBuyNow = calculateNextBid(maxBuyNow);
				if (cheapestBN > 0 && cheapestBN < Integer.MAX_VALUE) {
					keepSearching = false;
				}

			}

		}

		System.out.println("Cheapest BN for " + card.getName() + " is " + cheapestBN + " coins");
		// Collections.sort(set);
		return cheapestBN;

	}

	private SearchItem getSearchItem(long resourseId) {
		for (SearchItem c : cards) {
			if (c.getResourceId() == resourseId)
				return c;
		}
		return null;
	}

	private int findNearestValidBidDouble(double price) {
		return findNearestValidBid((int) price);
	}

	private int findNearestValidBid(int price) {

		if (price < 1000) {
			// round to nearest 50
			return ((price + 49) / 50) * 50;

		}
		if (price < 10000) {
			return ((price + 99) / 100) * 100;
		}
		if (price < 50000) {
			return ((price + 249) / 250) * 250;
		}
		if (price < 100000) {
			return ((price + 99999) / 100000) * 100000;
		}

		return 250;
	}

	public int calculateNextBid(ItemGson item) {
		int startingBid = item.getStartingBid();
		int currentBid = item.getCurrentBid();

		if (currentBid == 0)
			return startingBid;

		if (currentBid < 1000)
			return currentBid + 50;

		if (currentBid < 10000)
			return currentBid + 100;

		if (currentBid < 50000)
			return currentBid + 250;

		if (currentBid < 100000)
			return currentBid + 500;

		return currentBid + 1000;

	}

	public int calculateNextBid(int currentBid) {

		if (currentBid == 0)
			return 0;

		if (currentBid < 1000)
			return currentBid + 50;

		if (currentBid < 10000)
			return currentBid + 100;

		if (currentBid < 50000)
			return currentBid + 250;

		if (currentBid < 100000)
			return currentBid + 500;

		return currentBid + 1000;

	}

	/**
	 * For searching.. Search on 3000, will need 3100 bids
	 * 
	 * @param maxBid
	 * @return
	 */
	public int calculatePrevBid(int maxBid) {
		if (maxBid < 300)
			return 250;// Min bid

		if (maxBid < 1000)
			return maxBid - 50;

		if (maxBid < 10000)
			return maxBid - 100;

		if (maxBid < 50000)
			return maxBid - 250;

		if (maxBid < 100000)
			return maxBid - 500;

		return maxBid - 1000;

	}

	public boolean isRearGoldContract(ItemGson item) {
		if (item.getItemData().getResourceId() == 1615613742L) {
			if (item.getItemData().getRareflag() == 1) {
				return true;
			}
		}
		return false;

	}

	public void logToFile(String s) {
		try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(logFile, true)))) {
			out.println(s);
		} catch (IOException e) {
			// exception handling left as an exercise for the reader
		}
	}

	public void mySleep(int millisec) {

		try {
			Thread.sleep(millisec);// 1 sec
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param args
	 */
	public static ArrayList<SearchItem> getUpToDateCards() {
		ArrayList<SearchItem> list = new ArrayList<SearchItem>();
		// Keep selling them
		SearchItem contract = new SearchItem("GoldContract", SearchVar.Category_Contract, SearchVar.Type_Development, SearchVar.Level_Gold, null, 250, 0, 450,
				550, 1615613742L, 0);
		contract.setBuyMore(false);

		// BuyNow Cheapest is 850 (a few) 186115
		SearchItem gibbs = new SearchItem("Gibbs", 186115, 550, 800, 900, 1610798851);
		// cheapest BuyNow 900 186115
		SearchItem benteke = new SearchItem("Benteke", 184111, 650, 900, 1200, 1610796847);

		SearchItem clichy = new SearchItem("Clichy", 152554, 2100, 2600, 2900, 1610765290);
		// resId=1610765290
		// maskedDef 152554

		SearchItem vertongen = new SearchItem("Vertongen", 172871, 450, 750, 950, 1610785607);

		// maskedDef 172871
		// resId 1610785607

		// david luis
		// mask 179944
		// res id 1610792680
		SearchItem luiz = new SearchItem("DavidLuiz", 179944, 8000, 9300, 9900, 1610792680);

		// walker
		// mask 179944
		// res id 1610792680
		SearchItem walker = new SearchItem("kyleWalker", 188377, 2900, 3300, 3700, 1610801113);

		// ramires
		// mask 184943
		// resid 1610797679
		SearchItem ramires = new SearchItem("Ramires", 184943, 5000, 5600, 5900, 1610797679);

		list.add(contract);
		list.add(gibbs);
		list.add(benteke);
		list.add(clichy);
		list.add(vertongen);
		list.add(luiz);
		// list.add(walker);
		// list.add(ramires);
		Collections.shuffle(list);
		return list;
	}

	public static long getDefId(SearchItem item) {
		return item.getResourceId() - 1610612736;
	}

	public static void main(String[] args) {
		// SearchItem card = new SearchItem("Hunter",
		// SearchVar.Category_playStyle, SearchVar.Type_Training, null,
		// SearchVar.PlayStyle_Hunter, 3100, 3800, 4000,
		// 1615615847,0);
		Accounts accs = new Accounts();
		ArrayList<Account> list = accs.getAccList();

		ArrayList<SearchItem> l = getUpToDateCards();

		for (SearchItem c : l) {

			//System.out.println(c.getProfit() + " : " + c);
			//System.out.println("DefId:" + c.getMaskedDefId());
		}

		for (int i = 0; i < list.size(); i++) {
			System.out.println(" - " + i + " for starting :" + list.get(i));
		}
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();

		sc.close();
		if (n >= 0 && n < list.size()) {
			FutWorker futW = new FutWorker(list.get(n), l);
			Thread t2 = new Thread(futW);

			t2.start();
		} else {
			System.out.println("Wrong number");
		}


	}

}
