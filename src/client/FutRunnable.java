package client;

import helperClasses.Accounts;
import helperClasses.SearchItem;
import helperClasses.Accounts.Account;

import java.util.ArrayList;

import javax.swing.JTextArea;

import jsonEntities.ItemGson;

public class FutRunnable implements Runnable {
	private Core core;

	public static int sleep_30Sec = 30000;
	public static int sleep_OneMin = 60000;
	public static int sleep_TwoMin = 120000;

	private ArrayList<SearchItem> cards = new ArrayList<>();
	private Accounts accs;

	private Account myAcc;
	private SearchItem myCard;
	
	

	public FutRunnable(Account myAcc, SearchItem card) {
		this.myAcc = myAcc;
		this.myCard = card;
		

		cards.add(card);// Should give a list of cards to search for..

		//System.out.println(card);
		//System.out.println(card.getProfit());

		accs = new Accounts();

		printStatus("FutMain start");

		core = new Core(myAcc.getEmail(), myAcc.getPassword(), myAcc.getQuestion());
		core.login(false);

	}
	
	public void printStatus(String s){
		
		//System.out.println(s);
	}

	public void runBot() {

	}

	@Override
	public void run() {
		SearchItem card = myCard;

		mySleep(5000);

		boolean b = true;
		while (b) {
			searchAndBuy(card);
			mySleep(sleep_30Sec);
			core.keepAlive();
			mySleep(2000);
			reListAndMove();
			mySleep(sleep_OneMin);
			core.keepAlive();
			mySleep(sleep_OneMin);
		}

		//
		// core.
		core.keepAlive();

	}

	public void searchAndBuy(SearchItem card) {

		if (card.getMaxBid() >= core.getCredits()) {
			printStatus("Not enough money. No point in searching");
			return;
		}

		printStatus("Searching for " + card.getName());
		mySleep(333);
		int tradePileSize = core.getTradePile().size();
		printStatus("Tradepile size:" + tradePileSize);
		mySleep(333);
		int watchListSize = core.getWatchList().size();
		printStatus("Watchlist size:" + watchListSize);
		int tadeWatchSize = tradePileSize + watchListSize;
		mySleep(333);

		int currPage = 0; // Current page, increse by 16
		boolean keepSearching = true;

		while (keepSearching) {
			int searchMaxBid = calculatePrevBid(card.getMaxBid());
			ArrayList<ItemGson> items = core.searchAuctions(card.getCategory(), card.getType(), card.getLevel(), card.getPlayStyle(), currPage, searchMaxBid,card.getMaxBuyNow() ,card.getMaskedDefId());
			for (ItemGson item : items) {

				if (item.getItemData().getResourceId() != card.getResourceId()) {
					System.err.println("ID's are wrong!!");
				}

				if (tadeWatchSize > 29) {
					printStatus("Trade/Watchpile is full. Stopping searching");
					return;
				}
				if (accs.isMyAccount(item.getSellerName())) {
					printStatus("Dont bid, highest bidder is own account");
					return;
				}

				int bid = calculateNextBid(item);
				if (bid > core.getCredits()) {
					printStatus("Not enough money. Stopping seraching..");
					return;
				}
				if(item.getExpires()>1000){//Min 10 min left
					printStatus("No point on bidding, so long time left");
					return;
				}

				if (bid <= card.getMaxBid()) {
					printStatus("Bidding " + bid + " on " + item);
					boolean success = core.bid(item.getTradeId(), item.getItemData().getId(), bid);
					printStatus("Success :" + success);
					if (success)
						tadeWatchSize++;
					mySleep(3330);

				}
				// System.out.println(item);
			}

			currPage += 16;
			mySleep(3000);// 3 sec
			if (items.size() < 16) {
				keepSearching = false;
				printStatus("Finished searching.. No more items");
			}
		}
	}

	public void reListAndMove() {
		mySleep(500);
		ArrayList<ItemGson> tradepile = core.getTradePile();
		mySleep(500);
		ArrayList<ItemGson> watchlist = core.getWatchList();
		if(watchlist==null || tradepile==null){
			System.err.println("tradepile/watchpile is null");
			return;
		}
		printStatus("watchlist size :" + watchlist.size());
		// ------------ WatchList --------------------
		for (ItemGson item : watchlist) {

			// System.out.println(item);

			mySleep(1000);

			long tradeId = item.getTradeId();
			String tradeState = item.getTradeState();
			String bidState = item.getBidState();
			long itemId = item.getItemData().getId();

			if (bidState.equals("outbid") || (bidState.equals("none") && tradeState.equals("expired"))) {
				core.watchlistDelete(tradeId);
				printStatus("Removed item from watchlist");

			} else if (bidState.equals("highest") && tradeState.equals("closed")) {
				// Won it

				core.sendToTradepile(itemId, tradeId);
				printStatus("Sendt item to tradepile");

			} else {
				// Do nothing. Currently a bid on it..
			}

		}

		// ------------ Tradepile --------------------
		printStatus("Tradepile size :" + tradepile.size());
		for (ItemGson item : tradepile) {
			// System.out.println(item);
			long itemId = item.getItemData().getId();
			long tradeId = item.getTradeId();
			String tradeState = item.getTradeState();
			String bidState = item.getBidState();
			int currentBid = item.getCurrentBid();

			SearchItem card = getSearchItem(item.getItemData().getResourceId());
			if (card == null) {
				System.err.println("Card isnt in stock, dunno how to sell");
				return;
			}

			mySleep(500);

			if (tradeState == null || tradeState.equals("None")) {
				// Item was just bought and won, need to list it on market
				String s = "\t Item '" + card.getName() + "' was just bought and won, need to list it on market";
				printStatus(s);

				core.sell(itemId, card.getSellStart(), card.getSellBuyNow(), 3600);

			} else if (tradeState.equals("expired") && currentBid == 0) {
				// Item has expired, and wasnt sold. Need to relist
				String s = "\t Item '" + card.getName() + "' has expired, and wasnt sold. Need to relist";
				printStatus(s);

				core.sell(itemId, card.getSellStart(), card.getSellBuyNow(), 3600);

			} else if (tradeState.equals("closed")) {
				// Item has sold
				String s = "\t Sold item '" + card.getName() + "' for " + item.getCurrentBid() + " coins.";
				printStatus(s);
				core.tradePileDelete(tradeId);
			}

		}
	}
	
	
	public void calculatePrice(SearchItem card) {

		

		printStatus("Searching for " + card.getName());
		mySleep(333);
		
		
		int currPage = 0; // Current page, increse by 16
		boolean keepSearching = true;
		
		int searchPrice= 0;
		

		while (keepSearching) {
			int searchMaxBid = calculatePrevBid(card.getMaxBid());
			ArrayList<ItemGson> items = core.searchAuctions(card.getCategory(), card.getType(), card.getLevel(), card.getPlayStyle(), currPage, searchMaxBid, card.getMaxBuyNow(),card.getMaskedDefId());
			for (ItemGson item : items) {

				if (item.getItemData().getResourceId() != card.getResourceId()) {
					System.err.println("ID's are wrong!!");
				}

				
				
				

				
			}

			currPage += 16;
			mySleep(3000);// 3 sec
			if (items.size() < 16) {
				keepSearching = false;
				printStatus("Finished searching.. No more items");
			}
		}
	}

	private SearchItem getSearchItem(long resourseId) {
		for (SearchItem c : cards) {
			if (c.getResourceId() == resourseId)
				return c;
		}
		return null;
	}

	public int calculateNextBid(ItemGson item) {
		int startingBid = item.getStartingBid();
		int currentBid = item.getCurrentBid();

		if (currentBid == 0)
			return startingBid;

		if (currentBid < 1000)
			return currentBid + 50;

		if (currentBid < 10000)
			return currentBid + 100;

		if (currentBid < 50000)
			return currentBid + 250;

		if (currentBid < 100000)
			return currentBid + 500;

		return currentBid + 1000;

	}

	/**
	 * For searching.. Search on 3000, will need 3100 bids
	 * 
	 * @param maxBid
	 * @return
	 */
	public int calculatePrevBid(int maxBid) {
		if (maxBid < 300)
			return 250;// Min bid

		if (maxBid < 1000)
			return maxBid - 50;

		if (maxBid < 10000)
			return maxBid - 100;

		if (maxBid < 50000)
			return maxBid - 250;

		if (maxBid < 100000)
			return maxBid - 500;

		return maxBid - 1000;

	}

	public boolean isRearGoldContract(ItemGson item) {
		if (item.getItemData().getResourceId() == 1615613742L) {
			if (item.getItemData().getRareflag() == 1) {
				return true;
			}
		}
		return false;

	}

	public void mySleep(int millisec) {

		try {
			Thread.sleep(millisec);// 1 sec
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//SearchItem card = new SearchItem("Hunter", SearchVar.Category_playStyle, SearchVar.Type_Training, null, SearchVar.PlayStyle_Hunter, 3100, 3800, 4000,
		//		1615615847,0);
		
		SearchItem contract = new SearchItem("GoldContract", SearchVar.Category_Contract, SearchVar.Type_Development, SearchVar.Level_Gold, null, 250, 0, 450, 550, 1615613742L, 0);
		
		//BuyNow Cheapest is 850 (a few)
		SearchItem gibbs = new SearchItem("Gibbs", 186115, 650, 850, 900, 1610798851);
		//cheapest BuyNow 900 186115
		
		
		
		//System.out.println(benteke.getProfit()+" : "+benteke);
		

		
		
		//Thread t1 = new Thread(new FutRunnable(accs.askoy, gibbs));
		//t1.start();
		SearchItem benteke = new SearchItem("Benteke", 184111, 750, 900, 1200, 1610796847);
		Accounts accs = new Accounts();
		//Thread t2 = new Thread(new FutRunnable(accs.skanses,null));
		//t2.start();

	}

}
