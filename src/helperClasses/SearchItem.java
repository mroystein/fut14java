package helperClasses;

import client.SearchVar;

public class SearchItem {
//(String category,String type, String level,int start, int maxBid) {
	private String category;
	private String type;
	private String level;
	private String playStyle;
	private int maxBid;
	private int maxBuyNow;
	private String name;
	
	
	int sellStart;
	int sellBuyNow;
	long resourceId;
	long maskedDefId;
	
	private boolean buyMore;
	
	long timeStamp;
	
	/*
	 * macr	800
maskedDefId	186115
num	16
start	0
type	player
	 */
	
	
	/**
	 * MaskedDefId   /assetId/ unique Id from db (i.e 184111) using in searching
	 * Resource Id only to check cards after.. for blue, black cards etc..
	 */
	
	
	
	
	/**
	 * Good for players
	 * @param name
	 * @param maxBid
	 * @param maxBuyNow
	 * @param sellStart
	 * @param sellBuyNow
	 * @param maskedDefAssetId
	 * @param resId
	 */
	public SearchItem(String name, int maxBid, int maxBuyNow, int sellStart, int sellBuyNow, long maskedDefAssetId, long resId){
		this.name=name;
		this.type = SearchVar.Type_Player;
		this.maxBid = maxBid;
		this.maxBuyNow=maxBuyNow;
		this.sellStart = sellStart;
		this.sellBuyNow = sellBuyNow;
		this.resourceId=resId;
		this.maskedDefId=maskedDefAssetId;
		this.buyMore=true;
		timeStamp=System.currentTimeMillis();
	}
	
	
	
	/**
	 *  Constructor for making a player card
	 * @param name
	 * @param maskedDefId
	 * @param maxBid
	 * @param sellStart
	 * @param sellBuyNow
	 * @param resId
	 */
	public SearchItem(String name, long maskedDefId, int maxBid, int sellStart, int sellBuyNow,long resId){
		this(name,null,SearchVar.Type_Player,null,null,maxBid,0,sellStart,sellBuyNow,resId,maskedDefId);
		
		
	}
	
	public SearchItem(String name, long maskedDefId, int maxBuyNow,long resId){
		this(name,null,SearchVar.Type_Player,null,null,0,maxBuyNow,0,0,resId,maskedDefId);
		
		
	}
	
	
	public SearchItem(String name,String category, String type, String level, String playStyle, int maxBid,int maxBuyNow, int sellStart, int sellBuyNow,long resId,long maskedDefId) {
		this.name=name;
		this.category = category;
		this.type = type;
		this.level = level;
		this.playStyle = playStyle;
		this.maxBid = maxBid;
		this.maxBuyNow=maxBuyNow;
		this.sellStart = sellStart;
		this.sellBuyNow = sellBuyNow;
		this.resourceId=resId;
		this.maskedDefId=maskedDefId;
		this.buyMore=true;
		timeStamp=System.currentTimeMillis();
	}
	
	
	
	public long getTimeStamp() {
		return timeStamp;
	}
	
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public void setBuyMore(boolean buyMore) {
		this.buyMore = buyMore;
	}
	
	public boolean isBuyMore() {
		return buyMore;
	}
	
	
	public String getProfit(){
		double minProfit = sellStart*0.95-maxBid;
		double maxProfit = sellBuyNow*0.95-maxBid;
		return "Profit "+minProfit+" - "+maxProfit;
	}
	
	//SearchItem [category=null, type=player, level=null, playStyle=null, maxBid=3000, name=kyleWalker, sellStart=3300, sellBuyNow=3700]
	@Override
	public String toString() {
		if(type.equals(SearchVar.Type_Player)){
			return String.format("[%s, maxBid=%s, maxBN=%s, sellStart=%s, sellBytNow=%s]", name,maxBid, maxBuyNow,sellStart,sellBuyNow);
		}
		
		return "SearchItem [category=" + category + ", type=" + type + ", level=" + level + ", playStyle=" + playStyle + ", maxBid=" + maxBid + ", name="
				+ name + ", sellStart=" + sellStart + ", sellBuyNow=" + sellBuyNow + "]";
	}
	
	public int getMaxBuyNow() {
		return maxBuyNow;
	}
	public void setMaxBuyNow(int maxBuyNow) {
		this.maxBuyNow = maxBuyNow;
	}
	
	public long RESOURCE_CONSTANT = 1610612736;
	public long getMaskedDefId() {
		
		if(!getType().equals(SearchVar.Type_Player)){
			return maskedDefId;
			//Cannot check for other than players.. ? 
		}
		
		long genDefId = getResourceId()- RESOURCE_CONSTANT;
		if(genDefId!=maskedDefId){
			System.err.println("Wrong maskedId vs resId :"+getName());
			return 0;
			
		}else{
			//System.out.println("correct ids");
		}
		return maskedDefId;
	}
	public void setMaskedDefId(long maskedDefId) {
		this.maskedDefId = maskedDefId;
		
	}
	
	public static long getDefId(SearchItem item){
		return item.getResourceId()- 1610612736;
	}
	
	public long getResourceId() {
		return resourceId;
	}
	public void setResourceId(long resourceId) {
		this.resourceId = resourceId;
	}
	
	@Override
		public boolean equals(Object obj) {
			if(obj==null){
				return false;
			}
			if(!(obj instanceof SearchItem)){
				return false;
			}
			SearchItem card2 = (SearchItem) obj;
			
			return getResourceId()==card2.getResourceId();
		}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getPlayStyle() {
		return playStyle;
	}
	public void setPlayStyle(String playStyle) {
		this.playStyle = playStyle;
	}
	public int getMaxBid() {
		return maxBid;
	}
	public void setMaxBid(int maxBid) {
		this.maxBid = maxBid;
	}
	public int getSellStart() {
		return sellStart;
	}
	public void setSellStart(int sellStart) {
		this.sellStart = sellStart;
	}
	public int getSellBuyNow() {
		return sellBuyNow;
	}
	public void setSellBuyNow(int sellBuyNow) {
		this.sellBuyNow = sellBuyNow;
	}
	
	
	
}
