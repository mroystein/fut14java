package helperClasses;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Accounts {
	
	private ArrayList<Account> accList;

	public Accounts() {
		accList = new ArrayList<Account>();
		try {
			Scanner sc = new Scanner(new File("accs.txt"));
			while(sc.hasNextLine()){
				String[] s = sc.nextLine().split(",");
				//System.out.println(s.length);
				Account a = new Account(s[0], s[1], s[2], s[3]);
				accList.add(a);
			}
			sc.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	public ArrayList<Account> getAccList() {
		return accList;
	}

	public boolean isMyAccount(String clubName) {
		for (Account acc : accList) {
			if (acc.getClubName().equals(clubName)) {
				return true;
			}
		}
		return false;
	}

	public Account[] getAccountsArr() {
		Account[] arr = new Account[accList.size()];
		for (int i = 0; i < accList.size(); i++) {
			arr[i] = accList.get(i);
			
		}
		return arr;
	}
	
	public static void main(String[] args) {
		Accounts accs = new Accounts();
		for (Account a : accs.getAccList()) {
			System.out.println(a);
		}
	}

	public class Account {
		private String email;
		private String password;
		private String question;
		private String clubName;

		public Account(String email, String password, String question, String clubName) {

			this.email = email;
			this.password = password;
			this.question = question;
			this.clubName = clubName;

		}

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return email + " : " + clubName;
		}

		public String getClubName() {
			return clubName;
		}

		public void setClubName(String clubName) {
			this.clubName = clubName;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getQuestion() {
			return question;
		}

		public void setQuestion(String question) {
			this.question = question;
		}
	}
}
